import Vue from 'vue'
import Vuex from 'vuex'
import exam from './modules/exam'
import course from './modules/course'
import user from './modules/user'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    exam,
    course,
    user
  },
  plugins: [createPersistedState()]
})
// var store=new Vuex.Store({
//   state:{
      
//   },
//   mutations:{
//   },
//   getters:{

//   },
//   actions:{
//   },
//   modules:{
//     exam,
//     course,
//     user
//   },

//   Plugins:[createPersitedState()]
// })
// export default store
