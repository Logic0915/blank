const state = {
    examAddForm:{
        examName: '',
        examDate:'',
        totalTime:'',
        totalScore:'',
        examNote:'',
        uuIdList:[]
    },
    choiceProblemData: [],
    trueOrFalseProblemData: [],
    essayProblemData: [],
}
const mutations = {
    // // 添加题目到试卷
    // addChoiceProblemToExam(state, choiceProblemData) {
    //     // console.log(choiceProblemData)
    //     state.choiceProblemData = choiceProblemData;
    //     // console.log(state.choiceProblemData)
    // },

    // 添加题目到试卷
    addProblemToExam(state,payload) {
        console.log(payload)
        if(payload.problemType=="选择题"){
            state.choiceProblemData = payload.ProblemData;
        }
        else if(payload.problemType=="判断题"){
            state.trueOrFalseProblemData = payload.ProblemData;
        }
        else if(payload.problemType=="简答题"){
            state.essayProblemData = payload.ProblemData;
        }
        // console.log(state.choiceProblemData)
    },

    // // 添加题目到试卷
    // addProblemToExam(state,data) {
    //     console.log(data)
    // },
    // 用于清空题目
    setExamAddEmpty(state) {
        state.choiceProblemData = []
        state.trueOrFalseProblemData = []
        state.essayProblemData = []
        state.examAddForm = {
            examName: '',
            courseName:'大学英语',
            examDate:'',
            totalTime:'',
            totalScore:'',
            examNote:'',
            uuIdList:[]
        }
    },
    // 删除试卷中的某个选择题
    deleteExamChoiceItem(state, choiceId){
        let index = state.choiceProblemData.findIndex(choiceProblemData => choiceProblemData.choiceId === choiceId);
        console.log(index)
        if(index > -1)
            state.choiceProblemData.splice(index, 1);
    }
}
export default {
    namespaced: true,
    state,
    mutations
}