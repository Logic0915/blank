const state = {
    course:{
        courseId:2,
        courseName:'大学英语',
    }
}
const mutations = {
    updateCourse(state, course) {
        console.log(course)
        state.course = course
        router.push({name:' '})
        router.go(-1)
    }
}
import router from'@/router/index.js'
export default {
    namespaced: true,
    state,
    mutations
}