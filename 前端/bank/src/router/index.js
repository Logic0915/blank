import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home';
import Problem from '@/components/Problem';
import Exam from '@/components/Exam';
import ProblemAdd from '@/components/ProblemAdd';
import ProblemAuditList from '@/components/ProblemAuditList';
import ExamAdd from '@/components/ExamAdd';
import Tag from '@/components/Tag';
import ExamEdit from '@/components/ExamEdit';
import ExamAddProblem from '@/components/ExamAddProblem';
import ExamEditProblem from '@/components/ExamEditProblem';
import UserManage from '@/components/UserManage';
import TagValue from '@/components/TagValue';
import EditChoice from '@/components/EditChoice';
import EditTrueOrFalse from '@/components/EditTrueOrFalse';
import EditEssay from '@/components/EditEssay';
import UserDetails from '@/components/UserDetails';

//配合store.state.user.username来使用
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: {
      name: 'home'
    }
  },
  {
    path: '/home',
    name: 'home',
    meta: {
      title: '首页'
    },
    component: Home
  },
  
  // {
  //   path: '/curriculum',
  //   name: 'curriculum',
  //   meta: {
  //     title: '课程',
  //     requiresAuth:true
  //   },
  //   component: () => import(/* webpackChunkName: "user" */'../components/Curriculum.vue')
  // },  
  {
    path: '/login',
    name: 'login',
    meta: {
      title: '登录'
    },
    component: () => import('../components/UserLogin.vue')
  },
  {
    path: '/register',
    name: 'register',
    meta: {
      title: '注册'
    },
    component: () => import('../components/UserRegister.vue')
  },
  {
    path: '/menu',
    name: 'menu',
    meta: {
      keepAlive: true,
      title: '侧边栏菜单'
    },
    component: () => import('../components/Menu.vue'),
  },
  {
    path: '/main',
    name: 'main',
    meta: {
      title: '题目管理'
    },
    component: () => import('../views/Main.vue'),
    children:[
      {
        path:'/user/:name',
        name:'UserDetails',
        meta:{
          title:'简答题修改页面',
          requiresAuth:true
        },
        component:UserDetails
      },
      {
        path: '/problem',
        name: 'problem',
        meta: {
          title: '题目管理页面',
          requiresAuth:true
        },
        component: Problem
      },
      {
        path: '/exam',
        name: 'exam',
        meta: {
          title: '试卷管理页面',
          requiresAuth:true
        },
        component: Exam
      },
      {
        path: '/problemAdd',
        name: 'problemAdd',
        meta: {
          title: '新增题目页面',
          requiresAuth:true
        },
        component: ProblemAdd
      },
      {
        path: '/problemAuditList',
        name: 'problemAuditList',
        meta: {
          title: '新增题目页面',
          requiresAuth:true
        },
        component: ProblemAuditList
      },
      {
        path: '/examAdd',
        name: 'examAdd',
        meta: {
          title: '新增试卷页面',
          requiresAuth:true
        },
        component: ExamAdd
      },
      {
        path: '/tag',
        name: 'tag',
        meta: {
          title: '新增试卷页面',
          requiresAuth:true
        },
        component: Tag
      },
      {
        path: '/examEdit',
        name: 'examEdit',
        meta: {
          title: '修改试卷页面',
          requiresAuth:true
        },
        component: ExamEdit
      },
      {
        path: '/examAddProblem',
        name: 'examAddProblem',
        meta: {
          title: '创建试卷并添加选择题目页面',
          requiresAuth:true
        },
        component: ExamAddProblem
      },
      {
        path: '/examEditProblem',
        name: 'examEditProblem',
        meta: {
          // keepAlive: true,
          title: '试卷添加选择题目页面',
          requiresAuth:true
        },
        component: ExamEditProblem
      },
      {
        path: '/userManage',
        name: 'userManage',
        meta: {
          // keepAlive: true,
          title: '用户管理页面',
          requiresAuth:true
        },
        component: UserManage
      },
      {
        path: '/tagValue',
        name: 'tagValue',
        meta: {
          // keepAlive: true,
          title: '标签值页面',
          requiresAuth:true
        },
        component: TagValue
      },
      {
        path: '/EditChoice',
        name: 'EditChoice',
        meta: {
          title: '选择题修改页面',
          requiresAuth:true
        },
        component: EditChoice
      },
      {
        path: '/EditTrueOrFalse',
        name: 'EditTrueOrFalse',
        meta: {
          title: '判断题修改页面',
          requiresAuth:true
        },
        component: EditTrueOrFalse
      },
      {
        path: '/EditEssay',
        name: 'EditEssay',
        meta: {
          title: '简答题修改页面',
          requiresAuth:true
        },
        component: EditEssay
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to,from,next)=>{
  if(to.matched.some(record=>record.meta.requiresAuth )){
      if(store.state.user.user&&sessionStorage.getItem("token")){
          next()
      }else{
          next({
              path:'/login',
              query:{redirect:to.fullPath}
          })
      }
  }else{
      next()
  }
})

export default router
