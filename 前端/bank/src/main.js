import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuex from 'vuex'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import 'bootstrap/dist/js/bootstrap.js'
import 'bootstrap/dist/css/bootstrap.css'

Vue.use(VueAxios, axios) //安装插件
Vue.use(ElementUI)
Vue.use(Vuex)//安装插件
axios.defaults.baseURL = "/api"
Vue.config.productionTip = false



new Vue({
  router,
  render: h => h(App),
  store,
}).$mount('#app')
