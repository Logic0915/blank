﻿const webpack =require("webpack")
module.exports = {
  configureWebpack: {
    devtool: 'source-map',
    plugins: [
			// 配置 jQuery 插件的参数
			new webpack.ProvidePlugin({
				// 引入jquery
				$: 'jquery',
				jQuery: 'jquery',
				'window.jQuery': 'jquery'
			})
		]
  },
  devServer: {
    
    proxy: {
        // /api是后台数据接口的上下文路径
        '/api': {
          //这里的地址是后端数据接口的地址
          target: 'http://localhost:8082/',
          // target: 'http://111.229.37.167/',
          //允许跨域
          changOrigin: false,  //前后端代码在同一台机用false,不同电脑用true
          pathRewrite: {
            '^/api': ''
          }
      }
    }
    
  },
  productionSourceMap: process.env.NODE_ENV === 'production' ? false : true,
    css: {
      loaderOptions: {
          sass: {
              // 全局sass变量	
              //sass-loader 8.0.0以前版本（包含）
              prependData: `@import "~@/assets/scss/index.scss";`
          }
      }
  },
}