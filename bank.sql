drop database bank;
create database bank;
use bank;

/*
 Navicat Premium Data Transfer

 Source Server         : mysql1
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : bank

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 15/11/2022 15:59:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for choice
-- ----------------------------
DROP TABLE IF EXISTS `choice`;
CREATE TABLE `choice`  (
  `choiceId` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uuId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `courseId` int(0) NOT NULL COMMENT '科目id外键',
  `typeId` int(0) NOT NULL COMMENT '类型id外键',
  `choiceProblem` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '选择题题目内容',
  `choiceOp1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '选择题选项1：a',
  `choiceOp2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '选择题选项2：b',
  `choiceOp3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '选择题选项3：c',
  `choiceOp4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '选择题选项4：d',
  `choiceAnsw` enum('A','B','C','D') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '选择题答案：A、B、C、D',
  `choiceParse` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '选择题解析',
  `choiceDifficulty` enum('简单','中等','困难') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '简单' COMMENT '选择题难度：简单、中等、困难',
  `choiceAudit` enum('未审核','已审核') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '未审核' COMMENT '选择题审核状态：未审核、已审核',
  `choiceDelete` enum('true','false') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'false' COMMENT '是否软删除：false、true',
  PRIMARY KEY (`choiceId`, `choiceOp4`) USING BTREE,
  INDEX `fk_course_choice_1`(`courseId`) USING BTREE,
  INDEX `fk_type_choice_1`(`typeId`) USING BTREE,
  INDEX `uuId`(`uuId`) USING BTREE,
  CONSTRAINT `fk_course_choice_1` FOREIGN KEY (`courseId`) REFERENCES `course` (`courseId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_type_choice_1` FOREIGN KEY (`typeId`) REFERENCES `type` (`typeId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 306 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of choice
-- ----------------------------
INSERT INTO `choice` VALUES (1, '60d9ad305b6611ed904ef80dac0b7f41', 1, 1, '下列函数为偶函数的是【】', 'y=x sin x', 'y=x cos x', 'y=sin x+cos x', 'y=x(sinx+cos x)', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (2, '956079be5b6611ed904ef80dac0b7f41', 4, 1, '在通有电流的金属或半导体材料上施加磁场时，其电阻值将会发生明显的变化，此现象称为（      ）。', '电磁感应', '磁电感应', '磁电阻效应', '霍尔效应', 'C', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (3, '95608bc35b6611ed904ef80dac0b7f41', 4, 1, '对于磁性材料，在有外磁场作用时其电阻率较无磁场作用时方式巨大变化的现象称为（      ）。', '磁阻效应', '巨磁阻效应', '压阻效应', '霍尔效应', 'B', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (4, '956099145b6611ed904ef80dac0b7f41', 4, 1, '为增大霍尔元件灵敏度，霍尔元件多采用（       ）材料制作而成。', '金属', '绝缘', 'N型半导体', 'P型半导体', 'A', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (5, '9560a3e45b6611ed904ef80dac0b7f41', 4, 1, '干簧管是一种对（       ）敏感的特殊开关。', '磁场', '电场', '温度', '压力', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (6, '9560aed35b6611ed904ef80dac0b7f41', 4, 1, '磁电感应式传感器的输出电势大小正比于线圈相对于磁场的（       ）。', '相对位移', '相对速度', '相对电流', '相对电压', 'B', '暂无解析', '简单', '未审核', 'true');
INSERT INTO `choice` VALUES (7, 'c1cd784f5b6611ed904ef80dac0b7f41', 4, 1, '开关型霍尔传感器输出的为（      ）。', '模拟量', '数字量', '频率量', '位移量', 'B', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (8, 'c1cd855b5b6611ed904ef80dac0b7f41', 4, 1, '当载流导体或半导体处于与电流相垂直的磁场中时，在其两端产生电位差的现象称为（      ）。', '电场感应', '磁电感应', '霍尔效应', '涡流效应', 'C', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (9, 'c1cd92685b6611ed904ef80dac0b7f41', 4, 1, '霍尔元件直接输出的是（       ）信号。', '电流', '电压', '频率', '磁感应强度', 'B', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (10, 'c1cd9d9e5b6611ed904ef80dac0b7f41', 4, 1, '磁敏晶体管是一种（       ）转换器件。', '电磁', '磁电', '光电', '压电', 'B', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (11, 'cfe0ebee5b6611ed904ef80dac0b7f41', 4, 1, '下列器件中，不属于磁敏传感器的是（       ）。', '干簧管', '磁敏晶体管', '霍尔器件', '电阻应变片', 'D', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (12, 'cfe1110b5b6611ed904ef80dac0b7f41', 2, 1, 'Everything____into consideration, I propose that the first prize should be given to Liu Qiang.', 'to take', 'taking', 'taken', 'took', 'C', '从题目来看“I propose that the first prize should be given to Liu Qiang.”是主句，而“everything”与“take into consideration”之间是被动关系，所以只能选C。', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (13, 'cfe122715b6611ed904ef80dac0b7f41', 2, 1, 'I meant____ you about it, but I forgot to do so.', 'telling', 'having told', 'to tell', 'to have told', 'D', '首先考虑到“mean to”的结构，所以先排除A、B两项。而且此处是讲述一件本来要做而没做的事情，要用虚拟语气，所以只能选D。', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (14, 'cfe133175b6611ed904ef80dac0b7f41', 2, 1, 'While doing calculation for the project, the designers____a new solution to a geological problem.', 'fell into', 'stumbled upon', 'set out', 'discovered', 'B', '“fall into”含义为“被卷入……”;“stumble upon”含义为“偶然找到，发现”;“set out”含义为“启程，出发”;“discover”含义为“发现，发觉”。从四个选项看，含有“偶然”含义的“stumble upon”最适合本句话。', '困难', '未审核', 'true');
INSERT INTO `choice` VALUES (15, 'e09f8ad45b6611ed904ef80dac0b7f41', 2, 1, 'The mother almost____when she learned that her son was shot dead.', 'fell down', 'came down', 'fell apart', 'went to pieces', 'D', '“fall down”含义“跌倒，倒下”;“come down”含义为“降下，跌落” ;“fall apart”含义为“破裂，破碎”;“go to pieces”含义为“(身体或精神上)垮下来”。从四个选项来看，只有D适用于本句话。', '困难', '已审核', 'false');
INSERT INTO `choice` VALUES (16, 'e4df49a15b6611ed904ef80dac0b7f41', 2, 1, 'The matter is not to be ____.', 'watched for', 'waited on', 'taken over', 'trifled with', 'D', '“watch for”的含义为“提防，戒备”;“wait on”的含义为“服侍(某人)，招待(顾客)”;“take over”的含义为“接管(什么东西)”;“trifle with”含义为“轻视。”从四个选项来看，只有D适用于本句话。', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (17, 'e4df58755b6611ed904ef80dac0b7f41', 2, 1, 'They will never reconcile themselves to____.', 'defeat', 'their defeat', 'be defeated', 'have their defeat', 'B', '“reconcile sb. to”中后接名词，而“defeat”一词作为“失败”是可数名词，所以此处只能选择B。', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (18, 'e4df67065b6611ed904ef80dac0b7f41', 2, 1, 'You’d better let me know as soon as there is a(n)____position in the branch office.', 'empty', 'vacuum', 'hollow', 'vacant', 'D', '“empty”含义为“空无一物的”;vacuum是名词，含义为“真空，空虚”;“hollow”含义为“空洞的，中空的”，只有“vacant”与“position”搭配意为“空缺职位”。', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (19, 'e4df76e55b6611ed904ef80dac0b7f41', 2, 1, 'Urban congestion would greatly be relieved if the____charged on public transport were more reasonable.', 'prices', 'tickets', 'fees', 'fares', 'D', '“prices”的含义是“价格”，“tickets”是“票据”，“fees”是“费(如会费、学费、手续费等)”;“fares”主要是指“车船费”。所以此处用“fares”。', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (20, 'e4df84695b6611ed904ef80dac0b7f41', 2, 1, '题目111111111111111', '213', '213213', '213123', 'sdsad123', 'A', '解析9999', '困难', '已审核', 'true');
INSERT INTO `choice` VALUES (21, 'f5d855255b6611ed904ef80dac0b7f41', 2, 1, 'The physicists propose that our attention____the use of special methods of thinking and acting.', 'would be directed towards', 'should be directed towards', 'is directed towards', 'directs towards', 'B', '由“propose”一类词引导的从句要用虚拟语气，也就是用“should+动词原形”或者是直接用“动词原形”。所以此处选B。', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (22, 'f5d864f25b6611ed904ef80dac0b7f41', 2, 1, 'Animal mothers are devoted to their young and____ them with love and discipline.', 'mind', 'watch', 'help', 'tend', 'D', '“mind”表示”照看”时指的是“当其父母不在时照顾孩子”;“watch”表示“监视”的意思;“help”是帮助，只有“tend”是“照顾，照看”的意思。', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (23, 'f5d86fa85b6611ed904ef80dac0b7f41', 2, 1, 'To meet the ever increasing demand____oil-refining equipment, the company will produce more of such equipment.', 'of', 'on', 'with', 'for', 'B', '在选项中，A和D都可以与“demand”搭配，但是“meet the demand of”的含义为“满足(某人等)的需要”，而“meet the demand for”的含义为“满足对……的需要”。', '困难', '未审核', 'true');
INSERT INTO `choice` VALUES (24, 'f5d87df95b6611ed904ef80dac0b7f41', 2, 1, 'In selecting a material, the engineer’s interest is in its properties，which determine how it will perform under the loads and condition____it is subject.', 'where', 'which', 'in which', 'to which', 'D', '暂无解析', '困难', '已审核', 'false');
INSERT INTO `choice` VALUES (25, 'f5d889535b6611ed904ef80dac0b7f41', 2, 1, 'A new plane is going to ______ by us.', 'make', 'be make', 'made', 'be made', 'D', '暂无解析', '简单', '未审核', 'true');
INSERT INTO `choice` VALUES (26, '31168d3b60c811edb68cf80dac0b7f41', 2, 1, '_______!We can go camping.', 'How good weather ', 'What a good weather', 'How good the weather', 'What good weather', 'B', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (27, '3116be2260c811edb68cf80dac0b7f41', 1, 1, '函数f(x)=(x^2+1)cosx是（）', '奇函数', '偶函数', '有界函数', '周期函数', 'B', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (28, '3116ca5560c811edb68cf80dac0b7f41', 1, 1, '设函数f(x)=|x|,则函数在x=0处是()', '可导但不连续', '不连续且不可导', '连续且可导', '连续但不可导', 'D', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (29, '3116d9e060c811edb68cf80dac0b7f41', 1, 1, '方程z=x^2+y^2表示的二次曲面是()', '椭球面', '柱面', '圆锥面', '抛物面', 'C', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (30, '3116e60760c811edb68cf80dac0b7f41', 1, 1, '设f(x)在[a,b]上连续，在(a,b)内可导，f(a)=f(b),则在(a,b)内，曲线y=f(x)上平行于x轴的切线()', '至少有一条', '仅有一条', '不一定存在', '不存在', 'A', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (31, '4a66555460c811edb68cf80dac0b7f41', 1, 1, '若抛物线y=ax^2与曲线y=lnx相切，则a等于（）', '1', '1/2', '1/2e', '2e', 'C', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (32, '4a66653660c811edb68cf80dac0b7f41', 1, 1, '设函数f（x）=xln2x在x0处可导，且f\'（x0）=2,则f（x0）等于（）', '1', 'e/2', '2/e', 'e', 'B', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (33, '4a667c6c60c811edb68cf80dac0b7f41', 1, 1, '曲线y=lnx在点（e，1）处的切线斜率k为（）', '0', '1/e', '1', 'e', 'B', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (34, '4a66913560c811edb68cf80dac0b7f41', 3, 1, '栈和队列的共同特点是（）', '只允许在端点处插入和删除元素', '都是先进后出', '都是先进先出', '没有共同点 ', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (35, '4a669bc360c811edb68cf80dac0b7f41', 3, 1, '用链接方式存储的队列，在进行插入运算时', '仅修改头指针  ', '头、尾指针都要修改', '仅修改尾指针 ', '头、尾指针可能都要修改', 'D', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (36, '5addc69b60c811edb68cf80dac0b7f41', 3, 1, '以下数据结构中哪一个是非线性结构？', '队列', ' 栈 ', '线性表', '二叉树', 'D', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (37, '5addd35860c811edb68cf80dac0b7f41', 3, 1, '设有一个二维数组A[m][n]，假设A[0][0]存放位置在644(10)，A[2][2]存放位置在676(10)，每个元素占一个空间，问A[3][3](10)存放在什么位置？脚注(10)表示用10进制表示。', '688', '678', '692', '696', 'C', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (38, '5addde7e60c811edb68cf80dac0b7f41', 3, 1, '树最适合用来表示', '有序数据元素', '无序数据元素', '元素之间具有分支层次关系的数据', '元素之间无联系的数据', 'C', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (39, '5addee4260c811edb68cf80dac0b7f41', 3, 1, '二叉树的第k层的结点数最多为', '2^k-1', '2K+1', '2K-1 ', '2^(k-1)', 'D', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (40, '5addfb5860c811edb68cf80dac0b7f41', 3, 1, '若有18个元素的有序表存放在一维数组A[19]中，第一个元素放A[1]中，现进行二分查找，则查找A［3］的比较序列的下标依次为', ' 1，2，3', '9，5，2，3', '9，5，3', '9，4，2，3', 'D', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (41, '66f52e5060c811edb68cf80dac0b7f41', 3, 1, '对n个记录的文件进行快速排序，所需要的辅助存储空间大致为', 'O（1）', ' O（n）　', 'O（1og2n）', ' O（n2）', 'C', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (42, '66f539d060c811edb68cf80dac0b7f41', 3, 1, '对于线性表（7，34，55，25，64，46，20，10）进行散列存储时，若选用H（K）=K %9作为散列函数，则散列地址为1的元素有（  ）个', '1', '2', '3', '4', 'D', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (43, '66f5425a60c811edb68cf80dac0b7f41', 3, 1, '设有6个结点的无向图，该图至少应有(     )条边才能确保是一个连通图', '5', '6', '7', '8', 'A', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (44, '66f54ebc60c811edb68cf80dac0b7f41', 3, 1, '下面关于线性表的叙述错误的是（   ）', '线性表采用顺序存储必须占用一片连续的存储空间', '线性表采用链式存储不必占用一片连续的存储空间', '线性表采用链式存储便于插入和删除操作的实现', '线性表采用顺序存储便于插入和删除操作的实现', 'D', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (45, '66f5573e60c811edb68cf80dac0b7f41', 3, 1, '设哈夫曼树中的叶子结点总数为m，若用二叉链表作为存储结构，则该哈夫曼树中总共有（  ）个空指针域。', '2m-1', '2m', '2m+1', '4m', 'B', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (46, '73b5c7bd60c811edb68cf80dac0b7f41', 3, 1, '设顺序循环队列Q[0：M-1]的头指针和尾指针分别为F和R，头指针F总是指向队头元素的前一位置，尾指针R总是指向队尾元素的当前位置，则该循环队列中的元素个数为（  ）。', 'R-F', ' F-R', '(R-F+M)％M', '(F-R+M)％M', 'C', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (47, '73b5d36560c811edb68cf80dac0b7f41', 3, 1, '设某棵二叉树的中序遍历序列为ABCD，前序遍历序列为CABD，则后序遍历该二叉树得到序列为（   ）。', 'BADC', 'BCDA', 'CDAB', ' CBDA', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (48, '73b5db3260c811edb68cf80dac0b7f41', 3, 1, '设某完全无向图中有n个顶点，则该完全无向图中有（  ）条边。', 'n(n-1)/2', 'n(n-1)', 'n^2 ', 'n^2-1', 'A', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (49, '73b5e5ec60c811edb68cf80dac0b7f41', 3, 1, '设某棵二叉树中有2000个结点，则该二叉树的最小高度为（  ）。', '9 ', '10', '11', '12', 'C', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (50, '73b5ee4c60c811edb68cf80dac0b7f41', 3, 1, '设某有向图中有n个顶点，则该有向图对应的邻接表中有（  ）个表头结点。', ' n-1', 'n', 'n+1', '2n-1', 'B', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (51, '80310d2360c811edb68cf80dac0b7f41', 3, 1, '设一组初始记录关键字序列(5，2，6，3，8)，以第一个记录关键字5为基准进行一趟快速排序的结果为（  ）。', '2，3，5，8，6', ' 3，2，5，8，6', '3，2，5，6，8', ' 2，3，6，5，8', 'C', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (52, '80311ccc60c811edb68cf80dac0b7f41', 4, 1, '下列器件中，不属于磁敏传感器的是（       ）。', '干簧管', '磁敏晶体管', '霍尔器件', '电阻应变片', 'D', '暂无解析', '困难', '已审核', 'false');
INSERT INTO `choice` VALUES (53, '8031296260c811edb68cf80dac0b7f41', 4, 1, '半导体气敏传感器加热器的加热温度一般为（       ）°C', '50~100', '200~400', '500~800', '900~1000', 'B', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (54, '8031354960c811edb68cf80dac0b7f41', 4, 1, '在通有电流的金属或半导体材料上施加磁场时，其电阻值将会发生明显的变化，此现象称为（      ）。', '电磁感应', '磁电感应', '磁电阻效应', '霍尔效应', 'C', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (55, '8031405360c811edb68cf80dac0b7f41', 4, 1, '超声波的波长与频率之间是（       ）的关系', '正比', '反比', '相等', '无关', 'B', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (56, '915647fc60c811edb68cf80dac0b7f41', 4, 1, '当温度上升时，光敏三极管、光敏二极管的暗电流将会（）', '上升', '下降', '不变', '无法确定', 'A', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (57, '9156588660c811edb68cf80dac0b7f41', 4, 1, '下列半导体气敏元件中，不属于N型材料的是（       ）', 'SnO2', 'ZnO', 'TiO', 'CrO3', 'D', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (58, '91566c8960c811edb68cf80dac0b7f41', 4, 1, '气体传感器是利用化学、（      ）效应将气体的成分、浓度转换为电信号的一类传感器。', '生物', '物理', '光电', '磁电', 'B', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (59, '9156808c60c811edb68cf80dac0b7f41', 4, 1, '在一定光照下，光敏电阻两端所加电压越高，流过光敏电阻的光电流就（        ）。', '越大', '越小', '不变 ', '无法确定', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (60, '9156a59160c811edb68cf80dac0b7f41', 4, 1, '固体电解质气敏传感器是一种以（       ）为电解质的化学电池', '离子导体', '分子导体', '质子导体', '电子导体', 'A', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (61, 'a0d9581360c811edb68cf80dac0b7f41', 4, 1, '在光电耦合器中，不能作为光敏器件的是（        ）。', '光电二极管', '光电三极管', '光电池', '发光二极管', 'D', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (62, 'a63782e3625111ed834cf80dac0b7f41', 1, 1, '曲线y=xlnx的平行于直线x-y+1=0的切线方程为（）', 'y=x-1', 'y=-(x+1)', 'y=(lnx-1)(x-1)', 'y=x', 'A', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (63, 'a6379d7e625111ed834cf80dac0b7f41', 1, 1, '设函数f(x)=|x|,则函数在点x=0处()', '连续且可导', '连续且可微', '连续不可导', '不连续不可微', 'C', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (64, 'a637a87d625111ed834cf80dac0b7f41', 1, 1, '点x=0是函数y=x^4的（）', '驻点但非极值点', '拐点', '驻点且是拐点', '驻点且是极值点', 'C', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (65, 'a637b304625111ed834cf80dac0b7f41', 1, 1, '函数y=1/(x^2+1)是()', '偶函数', '奇函数', '单调函数', '无界函数', 'B', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (66, 'a637c6e0625111ed834cf80dac0b7f41', 1, 1, '设f[sin(x/2)]=cosx+1,则f(x)为（）', '2x^2-2', '2-2x^2', '1+x^2', '1-x^2', 'A', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (67, 'bad74dfa625111ed834cf80dac0b7f41', 1, 1, '数列有界是数列收敛的（）', '充分条件', '必要条件', '充要条件', '既非充分也非必要', 'D', '暂无解析', '困难', '已审核', 'false');
INSERT INTO `choice` VALUES (68, 'bad7646c625111ed834cf80dac0b7f41', 1, 1, '下列命题正确的是（）', '发散数列必无界', '两无界数列之和必无界', '两发散数列之和必发散', '两收敛数列之和必收敛', 'D', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (69, 'bad77584625111ed834cf80dac0b7f41', 1, 1, '当x→1时，下列与无穷小（x-1)等价的无穷小是（）', 'x^2-1', 'x^3-1', '(x-1)^2', 'sin(x-1)', 'C', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `choice` VALUES (70, 'bad78db3625111ed834cf80dac0b7f41', 1, 1, '下列有跳跃间断点x=0的函数为（）', 'xarctan1/x', 'arctan1/x', 'tan1/x', 'cos1/x', 'B', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (71, 'bad79fce625111ed834cf80dac0b7f41', 3, 1, '采用邻接表存储的图的广度优先遍历算法类似于二叉树的 _ 。', '先序遍历', '中序遍历', '后序遍历', '按层遍历', 'D', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (72, 'cd0550d5625111ed834cf80dac0b7f41', 3, 1, '已知二维数组A[1: 4, 1: 6]采用列序为主序方式存储，每个元素占用4个存储单元，并且A[3，4]的存储地址为1234，元素A[1, 1]的存储地址是（）', '1178', '1190', '1278', '1290', 'A', 'A[1: 4, 1: 6] 表示4行6列\r\nA【3，4】：a11+2*4*4+6*4=1234\r\na11=1178', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (73, 'cd05643c625111ed834cf80dac0b7f41', 3, 1, '要连通具有n个顶点的有向图,至少需要()条边', 'n-1', 'n', 'n+1', '2n', 'B', '有向图是n，无向图是n-1', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (74, 'cd0571c9625111ed834cf80dac0b7f41', 3, 1, '一棵124个叶结点的完全二叉树,最多有()个结点', '247', '248', '249', '250', 'B', '叶子结点是双分支节点数加1。所以双分支节点数为123，单分支节点数为1或者0，最多则选择1。124+123+1=248.', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (75, 'cd0580ed625111ed834cf80dac0b7f41', 3, 1, '在数据结构中，与所使用的计算机无关的是数据的______ 结构。', '逻辑', '存储', '逻辑和存储', '物理', 'A', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (76, 'cd058df1625111ed834cf80dac0b7f41', 3, 1, '在计算机中存储数据时，不仅要存储各数据元素的值，而且还要存储______。', '数据的处理方法', '数据元素的类型', '数据元素之间的关系', ' 数据的存储方法', 'C', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (77, 'd89f1f98625111ed834cf80dac0b7f41', 3, 1, '在计算机的存储器中表示时，逻辑上相邻的两个元素对应的物理地址也是相邻的，这种存储结构称之为______。', '逻辑结构', '顺序存储结构', ' 链式存储结构', '以上都正确', 'B', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (78, 'd89f2cdc625111ed834cf80dac0b7f41', 3, 1, '算法的时间复杂度与______ 有关', '问题规模', '计算机硬件性能', ' 编译程序质量', '程序设计语言', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (79, 'd89f3546625111ed834cf80dac0b7f41', 3, 1, '算法分析的主要任务之一是分析______。', '算法是否具有较好的可读性', '算法中是否存在语法错误', ' 算法的功能是否符合设计要求', '算法的执行时间和问题规模之间的关系', 'D', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (80, 'd89f3fc8625111ed834cf80dac0b7f41', 3, 1, '导致栈上溢的操作是 ( )', '栈满时执行的出栈', '栈满时执行的入栈', '栈空时执行的出栈', '栈空时执行的入栈', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (81, 'd89f4e0d625111ed834cf80dac0b7f41', 2, 1, '-How are things with you Bill?-_________________.', 'Hello Sue', 'I’m terribly busy these days', 'Mind your own business', 'See you later', 'B', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (82, 'e0dc2d6d625111ed834cf80dac0b7f41', 2, 1, 'The baby is hungry but there’s ______ milk in the bottle.', 'little\r\n', 'a little', 'few', 'a few', 'A', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (83, 'e0dc434f625111ed834cf80dac0b7f41', 2, 1, 'The patient acted on the doctor’s ___ and finally recovered.', 'advices', '.advice', 'advise', 'advises', 'B', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (84, 'e0dc5538625111ed834cf80dac0b7f41', 2, 1, 'Nancy is ______ girl.', 'a eighteen-year-old', 'an eighteen-years-old', 'a eighteen-years-old', 'an eighteen-year-old', 'C', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (85, 'e0dc6f83625111ed834cf80dac0b7f41', 2, 1, 'Thirty per cent of meals ______________ by families in the USA are eaten at one of the big chains.', 'consuming', 'consumed', 'to consume', 'are consumed', 'D', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `choice` VALUES (86, 'e0dc861d625111ed834cf80dac0b7f41', 2, 1, 'Three-quarters of people like it. So that’s __________ 90% which is our target.', 'precisely', 'approximately', 'under', 'around', 'B', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (87, 'e9251d02625111ed834cf80dac0b7f41', 2, 1, 'She was convicted __________ murder.', 'to', 'in', 'of', 'as', 'C', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (88, 'e9252ac8625111ed834cf80dac0b7f41', 2, 1, 'Harry who had failed in the final exam had a great worry ______ his mind.', 'on', 'in', 'with', 'at', 'D', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (89, 'e92533ac625111ed834cf80dac0b7f41', 2, 1, '__________ she passes her exams she won’t get into university.', 'If', 'Unless', 'When', 'Because', 'A', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `choice` VALUES (90, 'e9253d89625111ed834cf80dac0b7f41', 2, 1, 'She promised to ____________ me off at the station tomorrow.', 'see', 'get', 'meet', 'carry', 'B', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `choice` VALUES (91, 'e9254afb625111ed834cf80dac0b7f41', 1, 1, '下列说法正确的是（）', '无穷小量是负无穷大量', '无穷小是非常小的数', '无穷大量就是+∞', '负无穷大是无穷大量', 'C', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `choice` VALUES (92, 'f13481fe625111ed834cf80dac0b7f41', 1, 1, '函数f（x)=(x^2-1)/x+1,当x→-1时的极限（）', '2', '-2', '∞', '极限不存在', 'B', '暂无解析', '简单', '未审核', 'false');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `courseId` int(0) NOT NULL AUTO_INCREMENT,
  `courseName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`courseId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, '高等数学');
INSERT INTO `course` VALUES (2, '大学英语');
INSERT INTO `course` VALUES (3, '数据结构');
INSERT INTO `course` VALUES (4, '传感器技术与应用');

-- ----------------------------
-- Table structure for essay
-- ----------------------------
DROP TABLE IF EXISTS `essay`;
CREATE TABLE `essay`  (
  `essayId` int(0) NOT NULL AUTO_INCREMENT,
  `uuId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `courseId` int(0) NOT NULL,
  `typeId` int(0) NOT NULL,
  `essayProblem` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `essayAnsw` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `essayParse` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `essayDifficulty` enum('简单','中等','困难') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '简单',
  `essayAudit` enum('未审核','已审核') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '未审核',
  `essayDelete` enum('false','true') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'false',
  PRIMARY KEY (`essayId`) USING BTREE,
  INDEX `fk_course_Essay_1`(`courseId`) USING BTREE,
  INDEX `fk_type_Essay_1`(`typeId`) USING BTREE,
  INDEX `uuId`(`uuId`) USING BTREE,
  CONSTRAINT `fk_course_Essay_1` FOREIGN KEY (`courseId`) REFERENCES `course` (`courseId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_type_Essay_1` FOREIGN KEY (`typeId`) REFERENCES `type` (`typeId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of essay
-- ----------------------------
INSERT INTO `essay` VALUES (1, '3d3569e8608f11ed8d2ef80dac0b7f41', 1, 3, '设函数f(x)=lnx,则f(x)-f(y)=()', 'f(x/y)', '根据对数函数的运算法则，f(x)-f(y)=lnx-lny=ln(x/y)=f(x/y),因此,f(x)-f(y)的值为：f(x/y)', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (2, 'ac09b3ec60c811edb68cf80dac0b7f41', 1, 3, '设f(x)=ax+b,且f(0)=-2,f(2)=2,则f[f(x)]的值为:', '4x-6', '由已知条件f(0)=-2,f(2)=2,可知f(x)=2x-2,因此,f[f(x)]=4x-6', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (3, 'ac09c0a160c811edb68cf80dac0b7f41', 1, 3, '设函数f(2x)=lnx,则df(x)/dx=', 'x/1', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (4, 'ac09cdca60c811edb68cf80dac0b7f41', 1, 3, '曲线y=x^3-3x^2-x的拐点坐标_____', '（1，-3）', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `essay` VALUES (5, 'ac09da0360c811edb68cf80dac0b7f41', 1, 3, '设平面II过点（1，0，-1）且与平面4x-y+2z-8=0平行,则平面II的方程为____', '4x-y=2z=2', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `essay` VALUES (6, 'ac09e68060c811edb68cf80dac0b7f41', 1, 3, '函数y=x+ex上点(0,1)处的切线方程是______', '2x-y+1=0', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (7, 'bb49d88760c811edb68cf80dac0b7f41', 1, 3, '设f(x-1)=x^2,则f(x+1)=_____', 'x^2+4x+4', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (8, 'bb49e90460c811edb68cf80dac0b7f41', 1, 3, '曲线x^2=6y-y^3在(-2,2)点切线的斜率为_______', '2/3', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (9, 'bb49fe0e60c811edb68cf80dac0b7f41', 1, 3, 'f(x)=x+2根号x在[0,4]上的最大值为____', '8', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (10, 'bb4a119a60c811edb68cf80dac0b7f41', 1, 3, '设函数f（x）=x|x|，则f’（0）=_______', '0', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (11, 'bb4a20f360c811edb68cf80dac0b7f41', 1, 3, '曲线y=lnx在点（e，1）处的切线斜率k为（）', '1/e', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (12, '4eed9b67609611ed8d2ef80dac0b7f41', 2, 3, 'when the antelope wakes up every morning, the first thing_________comes into his mind is', 'that', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `essay` VALUES (13, '4eedc68d609611ed8d2ef80dac0b7f41', 2, 3, 'I must be able to run ___ (fast) than the fastest lion', 'faster', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `essay` VALUES (14, '4eeddc20609611ed8d2ef80dac0b7f41', 2, 3, 'And at just the same time, the lion ____ (wake) out of his dream', 'wakes', '暂无解析', '困难', '已审核', 'false');
INSERT INTO `essay` VALUES (15, 'ce22dbd460c811edb68cf80dac0b7f41', 3, 3, '通常从四个方面评价算法的质量：_______、_______、________和_______', '正确性、可读性、健壮性、高效性', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (16, 'ce22e6d160c811edb68cf80dac0b7f41', 3, 3, '一个算法的时间复杂度为(n3+n2log2n+14n)/n2，其数量级表示为_______', 'O(n)', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `essay` VALUES (17, 'ce22f1a560c811edb68cf80dac0b7f41', 3, 3, '3.假定一棵树的广义表表示为A（C，D（E，F，G），H（I，J）），则树中所含的结点数为__________个，树的深度为_________，树的度为_______。', '9、3、3', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `essay` VALUES (18, 'ce23003d60c811edb68cf80dac0b7f41', 4, 3, '接触燃烧式气敏传感器一般可分为直接接触燃烧式和（       ）两大类', '催化接触燃烧式', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (19, 'ce230c8c60c811edb68cf80dac0b7f41', 4, 3, '通常所使用的湿度传感器按照使用湿敏元件的不同可分为湿敏电阻与（ ）等类型。', ' 湿敏电容 ', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `essay` VALUES (20, 'dcc3cb9660c811edb68cf80dac0b7f41', 4, 3, '热电阻按照使用材料的不同一般可分为铂热电阻和（ ）热电阻两大类', '铜 ', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `essay` VALUES (21, 'f8a1f67c625111ed834cf80dac0b7f41', 3, 3, '如果使用比较高效的算法判断单链表有没有环的算法中，至少需要几个指针？', '2个', '判断链表有没有环，可以用快慢指针来实现，两指针的移动速度不一样。如果相遇，则表示有环，否则表示无环', '简单', '未审核', 'true');
INSERT INTO `essay` VALUES (22, 'f8a21409625111ed834cf80dac0b7f41', 3, 3, '用链接方式存储的队列，在进行插入运算时', '头、尾指针可能都要修改', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (23, '020a2ddc625211ed834cf80dac0b7f41', 3, 3, '中序遍历二叉排序树所得到的序列是___________序列', '有序', '二叉排序树的性质： 按中序遍历二叉排序树，所得到的中序遍历序列是一个递增有序序列。', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (24, '020a3b2d625211ed834cf80dac0b7f41', 3, 3, '设指针变量p指向双向循环链表中的结点X，则删除结点X需要执行的语句序列为_____________________（设结点中的两个指针域分别为llink和rlink）。', 'p>llink->rlink=p->rlink; p->rlink->llink=p->rlink', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (25, '45d62352625211ed834cf80dac0b7f41', 3, 3, '设有一个顺序循环队列中有M个存储单元，则该循环队列中最多能够存储________个队列元素；当前实际存储________________个队列元素（设头指针F指向当前队头元素的前一个位置，尾指针指向当前队尾元素的位置）。', 'M-1，(R-F+M)%M', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (26, '45d63403625211ed834cf80dac0b7f41', 3, 3, '设顺序线性表中有n个数据元素，则第i个位置上插入一个数据元素需要移动表中_______个数据元素；删除第i个位置上的数据元素需要移动表中_______个元素。', 'n+1-i，n-i。', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (27, '45d63ea9625211ed834cf80dac0b7f41', 3, 3, '设有一个二维数组A[m][n]，假设A[0][0]存放位置在644(10)，A[2][2]存放位置在676(10)，每个元素占一个空间，问A[3][3]存放在什么位置？脚注(10)表示用10进制表示。', '692', '计算公式A[i][j]：A[0][0]+nj+i;\r\n644 + 2 * n + 2 = 676，则计算出：n=15。A[3][3]=644+3*15+3=692。答案选C。', '困难', '未审核', 'false');
INSERT INTO `essay` VALUES (28, '45d6515f625211ed834cf80dac0b7f41', 3, 3, '假定一棵树的广义表表示为A（C，D（E，F，G），H（I，J）），则树中所含的结点数为_________个，树的深度为___________，树的度为_________。', '9、3、3。', '树的度为树内各节点的度的最大值，故答案为：9、3、3。', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (29, '45d65aef625211ed834cf80dac0b7f41', 3, 3, '设一维数组中有n个数组元素，则读取第i个数组元素的平均时间复杂度为（ ）。', 'O(1)', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `essay` VALUES (30, '50bb4eec625211ed834cf80dac0b7f41', 3, 3, '二叉树的第k层的结点数最多为：', '2^(k-1)', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (31, '50bb5f7c625211ed834cf80dac0b7f41', 3, 3, '后缀算式9 2 3 ± 10 2 / - 的值为__________。中缀算式（3+4X）-2Y/3对应的后缀算式为______________________。', '3， 4 X * + 2 Y * 3 / -', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `essay` VALUES (32, '50bb7dfc625211ed834cf80dac0b7f41', 3, 3, '若用链表存储一棵二叉树时，每个结点除数据域外，还有指向左孩子和右孩子的两个指针。在这种存储结构中，n个结点的二叉树共有________个指针域，其中有________个指针域是存放了地址，有______个指针是空指针。', '2n, n-1, n+1', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (33, '50bb8e9b625211ed834cf80dac0b7f41', 3, 3, '向一棵B_树插入元素的过程中，若最终引起树根结点的分裂，则新树比原树的高度___________。', '增加1', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `essay` VALUES (34, '50bba34b625211ed834cf80dac0b7f41', 3, 3, '设哈夫曼树中的叶子结点总数为m，若用二叉链表作为存储结构，则该哈夫曼树中总共有（）个空指针域。', '2m', '哈夫曼树中只有N0和N2节点，如果用二叉链表来存储，度为2的结点的左右孩子都存在，没有空指针，度为0的叶子没有孩子，因此左右孩子的链域都为空，因此该Huffman树一共有2m个空指针。', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (35, '5a625a60625211ed834cf80dac0b7f41', 3, 3, '设某棵二叉树的中序遍历序列为ABCD，前序遍历序列为CABD，则后序遍历该二叉树得到序列为（ ）。', '后序遍历结果为 BADC。', '通过中序遍历和前序遍历可以将树构建出来，再求其后序遍历结果。\r\n前序遍历（先根排序），故C为根节点，再看中序遍历可知，AB为C的左子树，D为其右子树。AB - C - D\r\n前序遍历第二个节点为A，则A为根节点，再看中序遍历B在A后面，则B为右子树', '困难', '未审核', 'false');
INSERT INTO `essay` VALUES (36, '5a626708625211ed834cf80dac0b7f41', 3, 3, '设某棵二叉树中有2000个结点，则该二叉树的最小高度为（ ）。', '11', '满二叉树高度与节点个数关系是num=2n-1，则：210 < 2000 < 211。', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (37, '5a626ffa625211ed834cf80dac0b7f41', 3, 3, '设一棵完全二叉树中有500个结点，则该二叉树的深度为__________；若用二叉链表作为该完全二叉树的存储结构，则共有___________个空指针域。', '9、501', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `essay` VALUES (38, '5a62787b625211ed834cf80dac0b7f41', 3, 3, '设哈夫曼树中共有n个结点，则该哈夫曼树中有________个度数为1的结点。', '0', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (39, '5a6280e1625211ed834cf80dac0b7f41', 3, 3, '设有n个结点的完全二叉树，如果按照从自上到下、从左到右从1开始顺序编号，则第i个结点的双亲结点编号为____________，右孩子结点的编号为___________。', 'i/2，2i+1', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `essay` VALUES (40, '64a4af18625211ed834cf80dac0b7f41', 3, 3, '设一棵二叉树的深度为k，则该二叉树中最多有（ ）个结点。', '2^(k-1)', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (41, '64a4c235625211ed834cf80dac0b7f41', 3, 3, '在二叉排序树中插入一个结点的时间复杂度为（ ）。', 'O(n)', '最差情况下是O(n) 如果是最一般最基础的二叉树的话, 因为深度不平衡,所以会发展成单链的形状,就是一条线 n个点那么深，如果是深度平衡的二叉树 o(logn)。', '中等', '未审核', 'false');
INSERT INTO `essay` VALUES (42, '64a4d034625211ed834cf80dac0b7f41', 4, 3, '霍尔元件一般由（      ）、引线与壳体等组成。', '霍尔片', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (43, '64a4e632625211ed834cf80dac0b7f41', 4, 3, '根据霍尔传感器的输出特性，通常将霍尔传感器分为线性型、开关型与（      ）等三种类型。', '锁键型', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (44, '64a5014e625211ed834cf80dac0b7f41', 4, 3, '开关型霍尔传感器输出的为数字量，而线性型霍尔传感器输出的为（      ）。', '模拟量', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (45, '714c8e58625211ed834cf80dac0b7f41', 4, 3, '常用的磁敏晶体管有磁敏二极管和（       ）。', ' 磁敏三极管', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (46, '714c9c3f625211ed834cf80dac0b7f41', 4, 3, '超声波在介质中传播时，其能量随传播距离的增加逐渐减弱的现象称为', '衰减', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (47, '714caa02625211ed834cf80dac0b7f41', 4, 3, '超声波是指频率高于（      ）HZ的机械波。', '20000', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (48, '714cb6b9625211ed834cf80dac0b7f41', 4, 3, '超声波接收器是利用超声波的（      ）压电效应制作而成的', '正', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (49, '714cc9f6625211ed834cf80dac0b7f41', 4, 3, '超声波发射器是利用超声波的（      ）压电效应制作而成的', '逆', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (50, '7da2c2f4625211ed834cf80dac0b7f41', 4, 3, '超声波发射电路主要由（       ）、驱动电路与超声波发射探头等组成', '超声波振荡电路  /  超声波振荡器  /  超声波产生电路', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (51, '7da2d2d9625211ed834cf80dac0b7f41', 4, 3, '测量电阻应变变化时，通常采用（       ）电路', '电桥', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (52, '7da2dfb1625211ed834cf80dac0b7f41', 4, 3, '金属应变片有金属丝式和（       ）两种形式。', '箔式', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (53, '7da2ec89625211ed834cf80dac0b7f41', 4, 3, '半导体应变片主要是利用半导体材料的（       ）效应制作而成的一种电阻性元件。', '压阻', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (54, '7da2f8ca625211ed834cf80dac0b7f41', 2, 3, '- What do you do on Sunday?\r\n\r\n- _______', 'I play football. ', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `essay` VALUES (55, '8945913f625211ed834cf80dac0b7f41', 2, 3, '- Thank you for calling.\r\n\r\n- _________', '.Nice talking to you.', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `essay` VALUES (56, '89459ed9625211ed834cf80dac0b7f41', 2, 3, '- What do you think of this novel?\r\n\r\n- _________', 'It\'s well-written.', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (57, '8945a7bd625211ed834cf80dac0b7f41', 2, 3, '- How\'s your family?- _________', 'Not too bad.', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `essay` VALUES (58, '8945c477625211ed834cf80dac0b7f41', 2, 3, '- Let\'s go to the library this afternoon.\r\n\r\n- _________', 'That\'s a good idea.', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (59, '8945df53625211ed834cf80dac0b7f41', 2, 3, '- Excuse me, when will the 17:15 train arrive? - _________', 'It\'s been delayed one hour.', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `essay` VALUES (60, '93f8e0ce625211ed834cf80dac0b7f41', 2, 3, '- Let\'s go to the library this afternoon.\r\n\r\n- _________', 'That\'s a good idea.', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `essay` VALUES (61, '93f8f9e2625211ed834cf80dac0b7f41', 2, 3, '- How\'s your family?\r\n\r\n- _________', 'Not too bad.', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `essay` VALUES (62, '93f91316625211ed834cf80dac0b7f41', 2, 3, '- It\'s rather cold in here. Do you mind if I close the window?\r\n\r\n- ________', 'No, please.', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `essay` VALUES (63, '93f926dd625211ed834cf80dac0b7f41', 2, 3, '- What\'s the problem with your bike?\r\n\r\n- _________', 'Nothing serious.', '暂无解析', '简单', '已审核', 'false');

-- ----------------------------
-- Table structure for exam
-- ----------------------------
DROP TABLE IF EXISTS `exam`;
CREATE TABLE `exam`  (
  `examId` int(0) NOT NULL AUTO_INCREMENT,
  `examName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `courseId` int(0) NULL DEFAULT NULL,
  `examDate` datetime(0) NULL DEFAULT NULL,
  `totalTime` int(0) NULL DEFAULT NULL,
  `totalScore` int(0) NULL DEFAULT NULL,
  `state` enum('已开始','已结束','未发布','待选题') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '未发布',
  `examNote` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`examId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam
-- ----------------------------
INSERT INTO `exam` VALUES (1, '大学英语结课考试', 2, '2022-10-13 14:30:00', 90, 100, '已结束', '以选择题为主的试卷形式');
INSERT INTO `exam` VALUES (2, '大学英语结课考试模拟题', 2, '2022-12-01 09:00:00', 90, 100, '未发布', '选择题为主');
INSERT INTO `exam` VALUES (3, '高等数学考试模拟', 1, '2022-11-23 12:00:00', 120, 100, '未发布', '选择题为主');
INSERT INTO `exam` VALUES (4, '数据结构模拟题', 3, '2022-11-10 12:00:00', 120, 100, '未发布', '选择题判断题为主');
INSERT INTO `exam` VALUES (5, '传感器技术与应用模拟题', 4, '2022-11-10 12:00:00', 90, 100, '未发布', '选择题为主');
INSERT INTO `exam` VALUES (6, '高等数学考试', 1, '2022-11-02 12:00:00', 120, 100, '未发布', '简答题为主');
INSERT INTO `exam` VALUES (7, '数据结构结课考试', 3, '2022-11-26 12:00:00', 100, 100, '待选题', '判断题');
INSERT INTO `exam` VALUES (8, '传感器技术与应用期末考试', 4, '2022-11-18 12:00:00', 150, 120, '待选题', '选择题为主');
INSERT INTO `exam` VALUES (9, '高等数学期末考试', 1, '2022-11-12 12:00:00', 150, 100, '待选题', '待选');
INSERT INTO `exam` VALUES (10, '数据结构期中考试', 3, '2022-11-12 13:00:00', 120, 100, '未发布', '待选');
INSERT INTO `exam` VALUES (11, '传感器技术与应用期中考试', 4, '2022-11-19 12:00:00', 100, 100, '未发布', '待选');
INSERT INTO `exam` VALUES (12, '高等数学期中考试', 1, '2022-11-12 12:00:00', 100, 100, '未发布', '待选');
INSERT INTO `exam` VALUES (13, '大学英语期中考试', 2, '2022-11-11 12:00:00', 100, 150, '未发布', '待发布');
INSERT INTO `exam` VALUES (14, '大学英语期末考试', 2, '2022-11-10 12:00:00', 120, 150, '未发布', '待发布');
INSERT INTO `exam` VALUES (15, '大学英语模拟考试', 2, '2022-11-26 12:00:00', 100, 100, '未发布', '待发布');
INSERT INTO `exam` VALUES (16, '数据结构期中考试', 3, '2022-11-24 12:00:00', 120, 150, '未发布', '待发布');
INSERT INTO `exam` VALUES (17, '数据结构期末考试', 3, '2022-11-24 12:00:00', 120, 110, '未发布', '待选题');
INSERT INTO `exam` VALUES (18, '高等数学结课考试', 1, '2022-11-24 12:00:00', 150, 120, '未发布', '待选题');

-- ----------------------------
-- Table structure for examdata
-- ----------------------------
DROP TABLE IF EXISTS `examdata`;
CREATE TABLE `examdata`  (
  `examDataId` int(0) NOT NULL AUTO_INCREMENT,
  `examId` int(0) NULL DEFAULT NULL,
  `uuId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`examDataId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 179 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of examdata
-- ----------------------------
INSERT INTO `examdata` VALUES (167, 1, 'e4df67065b6611ed904ef80dac0b7f41');
INSERT INTO `examdata` VALUES (168, 1, 'e4df49a15b6611ed904ef80dac0b7f41');
INSERT INTO `examdata` VALUES (169, 1, '023aa83160c911edb68cf80dac0b7f41');
INSERT INTO `examdata` VALUES (170, 1, '69fe3b71609611ed8d2ef80dac0b7f41');
INSERT INTO `examdata` VALUES (171, 1, '4eedc68d609611ed8d2ef80dac0b7f41');
INSERT INTO `examdata` VALUES (172, 1, '4eeddc20609611ed8d2ef80dac0b7f41');
INSERT INTO `examdata` VALUES (173, 1, '4eed9b67609611ed8d2ef80dac0b7f41');
INSERT INTO `examdata` VALUES (174, 2, 'e0dc6f83625111ed834cf80dac0b7f41');
INSERT INTO `examdata` VALUES (175, 2, 'e0dc434f625111ed834cf80dac0b7f41');
INSERT INTO `examdata` VALUES (176, 2, 'e0dc2d6d625111ed834cf80dac0b7f41');
INSERT INTO `examdata` VALUES (177, 2, '93f91316625211ed834cf80dac0b7f41');
INSERT INTO `examdata` VALUES (178, 2, '93f8e0ce625211ed834cf80dac0b7f41');
INSERT INTO `examdata` VALUES (179, 1, 'f5d855255b6611ed904ef80dac0b7f41');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pwd` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `authority` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('Alice', '$2a$10$5ooQI8dir8jv0/gCa1Six.GpzAdIPf6pMqdminZ/3ijYzivCyPlfK', 1, 'ROLE_vip', NULL, NULL);
INSERT INTO `t_user` VALUES ('Mary', '$2a$10$5ooQI8dir8jv0/gCa1Six.GpzAdIPf6pMqdminZ/3ijYzivCyPlfK', 1, 'ROLE_common', NULL, NULL);
INSERT INTO `t_user` VALUES ('Mike', '$2a$10$5ooQI8dir8jv0/gCa1Six.GpzAdIPf6pMqdminZ/3ijYzivCyPlfK', 0, 'ROLE_vip', NULL, NULL);
INSERT INTO `t_user` VALUES ('Tom', '$2a$10$5ooQI8dir8jv0/gCa1Six.GpzAdIPf6pMqdminZ/3ijYzivCyPlfK', 0, 'ROLE_common', NULL, NULL);

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag`  (
  `tagId` int(0) NOT NULL AUTO_INCREMENT,
  `tagName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `parentTagId` int(0) NULL DEFAULT NULL,
  `courseId` int(0) NULL DEFAULT NULL,
  `tagColour` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `tagAudit` enum('未审核','已审核') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '未审核',
  PRIMARY KEY (`tagId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES (1, '历年真题', NULL, 2, '#1E90FF', '已审核');
INSERT INTO `tag` VALUES (2, '2021年真题', 1, 2, '#1E90FF', '已审核');
INSERT INTO `tag` VALUES (3, '2020年真题', 1, 2, '#1E90FF', '已审核');
INSERT INTO `tag` VALUES (4, '2019年真题', 1, 2, '#1E90FF', '已审核');
INSERT INTO `tag` VALUES (5, '八省联考真题', NULL, 2, '#C71585', '已审核');
INSERT INTO `tag` VALUES (6, '2018年真题', 1, 2, '#1E90FF', '已审核');
INSERT INTO `tag` VALUES (7, '五年高考三年模拟精选', NULL, 2, '#FF8C00', '已审核');
INSERT INTO `tag` VALUES (9, '广东真题', 6, 2, '#00CED1', '已审核');
INSERT INTO `tag` VALUES (10, '上海真题', 6, 2, '#00CED1', '已审核');
INSERT INTO `tag` VALUES (11, '高频真题', 9, 2, '#00CED1', '已审核');
INSERT INTO `tag` VALUES (12, '广东真题', 4, 2, '#1E90FF', '已审核');

-- ----------------------------
-- Table structure for tagvalue
-- ----------------------------
DROP TABLE IF EXISTS `tagvalue`;
CREATE TABLE `tagvalue`  (
  `tagValueId` int(0) NOT NULL AUTO_INCREMENT,
  `tagId` int(0) NOT NULL,
  `tagValue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tagValueAudit` enum('已审核','未审核') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '未审核',
  PRIMARY KEY (`tagValueId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tagvalue
-- ----------------------------
INSERT INTO `tagvalue` VALUES (12, 2, '广东省真题', '已审核');
INSERT INTO `tagvalue` VALUES (13, 2, '上海真题', '已审核');
INSERT INTO `tagvalue` VALUES (14, 6, '高频题', '已审核');
INSERT INTO `tagvalue` VALUES (15, 6, '压轴题', '已审核');
INSERT INTO `tagvalue` VALUES (16, 3, '广东真题', '已审核');

-- ----------------------------
-- Table structure for trueorfalse
-- ----------------------------
DROP TABLE IF EXISTS `trueorfalse`;
CREATE TABLE `trueorfalse`  (
  `trueOrFalseId` int(0) NOT NULL AUTO_INCREMENT,
  `uuId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `courseId` int(0) NOT NULL,
  `typeId` int(0) NOT NULL,
  `trueOrFalseProblem` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `trueOrFalseAnsw` enum('对','错') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '对、错',
  `trueOrFalseParse` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `trueOrFalseDifficulty` enum('简单','中等','困难') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '简单' COMMENT '简单、中等、困难',
  `trueOrFalseAudit` enum('未审核','已审核') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '未审核' COMMENT '未审核、已审核',
  `trueOrFalseDelete` enum('false','true') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'false' COMMENT 'false、true',
  PRIMARY KEY (`trueOrFalseId`) USING BTREE,
  INDEX `fk_course_trueorfalse_1`(`courseId`) USING BTREE,
  INDEX `fk_type_trueorfalse_1`(`typeId`) USING BTREE,
  CONSTRAINT `fk_course_trueorfalse_1` FOREIGN KEY (`courseId`) REFERENCES `course` (`courseId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_type_trueorfalse_1` FOREIGN KEY (`typeId`) REFERENCES `type` (`typeId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trueorfalse
-- ----------------------------
INSERT INTO `trueorfalse` VALUES (1, '3d35a867608f11ed8d2ef80dac0b7f41', 1, 2, '函数y=sinx是周期函数，以2Π为周期的函数()', '对', '函数y=sinx是周期函数，以2Π为周期。因此该表述是正确的', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (2, 'e90a534460c811edb68cf80dac0b7f41', 1, 2, '函数y=cosx是奇函数', '错', '函数y=cosx关于Y轴对称,因此,函数y=cosx是偶函数，所以原题的表达是错误的。', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (3, 'e90a5e8660c811edb68cf80dac0b7f41', 1, 2, '函数在开区间(0,1)内无界。', '对', '根据函数的有界性,可知该函数在指定区间内无界。可知该函数在指定区间内无界。因此该表述是正确的', '困难', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (4, 'e90a67b660c811edb68cf80dac0b7f41', 1, 2, '若变量存在极限,则极限唯一。', '对', '本题考查极限性质,根据极限的唯一性,可知该表述正确。', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (5, 'e90a74c760c811edb68cf80dac0b7f41', 1, 2, '使函数值为零的点，称为函数的零点。', '对', '本体考查零点的定义，根据定义，可知该表述正确。', '中等', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (6, 'e90a7d6260c811edb68cf80dac0b7f41', 1, 2, '闭区间上的连续函数是无界的', '错', '根据闭区间上连续函数的性质,闭区间上连续函数有界，因此该说法是错误的。', '困难', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (7, 'f563dbf360c811edb68cf80dac0b7f41', 1, 2, '无穷小量是一个很小的数。', '错', '无穷小量是极限为零的一类函数,不能把它与很小的数混为一谈,因此该说法错误', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (8, 'f563e90d60c811edb68cf80dac0b7f41', 1, 2, '无穷小量的唯一常数是零.', '对', '零是无穷小量的唯一常数,因此该表达是正确的', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (9, 'f563f32d60c811edb68cf80dac0b7f41', 1, 2, '单调有界数列必有极限。', '对', '极限存在准则II:单调有界数列必有极限。因此该表达是正确的。', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (10, 'f563fcf460c811edb68cf80dac0b7f41', 1, 2, '连续的函数一定有最大值和最小值。', '错', '根据连续函数的最值定理,在闭区间上的连续函数一定有最大值和最小值。因此，该表述是片面的。', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (11, 'f56407ec60c811edb68cf80dac0b7f41', 2, 2, 'Lingui stics is the systematic studey of language.', '对', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (12, '023a7e3360c911edb68cf80dac0b7f41', 2, 2, 'Lingui stics deals with a particular language.', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (13, '023a88bc60c911edb68cf80dac0b7f41', 1, 2, '函数f（x）=tanx是（0，Π/2）内的连续函数', '对', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (14, '023a928860c911edb68cf80dac0b7f41', 1, 2, '函数f（x）=-sinx^2、g（x）=-1+cosx^2是同一个函数。', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (15, '023a9b9460c911edb68cf80dac0b7f41', 2, 2, 'Lingui stics is scientific because it is helpful to language use.', '错', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (16, '69fdf984609611ed8d2ef80dac0b7f41', 2, 2, 'The task of a linguist is to discover the nature and rules of the underly language system.', '对', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (17, '69fe1492609611ed8d2ef80dac0b7f41', 2, 2, 'General linguis tics does not study theories of language.', '错', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (18, '69fe284d609611ed8d2ef80dac0b7f41', 2, 2, 'Phonology studies how a sound is produced', '错', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (19, '69fe3b71609611ed8d2ef80dac0b7f41', 2, 2, 'Morphology is the study of sentences.', '错', '暂无解析', '困难', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (20, '69fe46cc609611ed8d2ef80dac0b7f41', 2, 2, 'Syntas is the study of the rules of words.', '错', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (21, '023aa83160c911edb68cf80dac0b7f41', 2, 2, 'Semantics is the study of word meaning', '错', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (22, '0dcb25e360c911edb68cf80dac0b7f41', 2, 2, 'Pragrnatics is the study of meaning in context of language use', '对', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (23, '0dcb381260c911edb68cf80dac0b7f41', 2, 2, 'Socio linguics tics deals with the relation between language and society.', '对', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (24, '0dcb434160c911edb68cf80dac0b7f41', 3, 2, '将N个数据按照从小到大顺序组织存放在一个单向链表中。如果采用二分查找，那么查找的平均时间复杂度是O(logN)。', '错', '二分查找的平均复杂度是O（logN）没有错，一看到这个就跳坑了。然后知道陷阱来了！按顺序存放在单项链表中。二分查找是不可以用链表存储的：\n这是由链表的特性决定的。链表是很典型的顺序存取结构，数据在链表中的位置只能通过从头到尾的顺序检索得到，即使是有序的，要操作其中的某个数据也必须从头开始。\n这和数组有本质的不同。数组中的元素是通过下标来确定的，只要你知道了下标，就可以直接存储整个元素，比如a[5],是直接的。链表没有这个，所以，折半查找只能在数组上进行。\r\n', '困难', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (25, '0dcb523960c911edb68cf80dac0b7f41', 3, 2, '在单链表中，要访问某个结点，只要知道该结点的指针即可。因此，单链表是一种随机存取结构。', '错', '线性表分(顺序存储和链式存储)\r\n顺序存储即数组,我们使用数组的时候申请的是连续的内存空间可以直接读取的，a[24],a[25]\r\n链式存储即链表，链表中单个节点的内存地址不是连续的，而是散列在计算机中，通过next指针访问下一个节点，所以所必须遍历链表才能读取数据！', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (26, '0dcb641860c911edb68cf80dac0b7f41', 3, 2, '取线性表的第i个元素的时间同i的大小有关。', '错', '线性表分顺序表和链表\r\n顺序表最主要的特点是随机访问，即通过首地址和元素序号可以在O(1)的时间内找到指定的元素\r\n线性表因为是按序号直接取值，所以没有关系，但如果是链式存储结构就有关系', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (27, '18d90ab760c911edb68cf80dac0b7f41', 4, 2, '利用电感式传感器可以进行金属探测', '对', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (28, '18d91d7360c911edb68cf80dac0b7f41', 4, 2, '超声波传感器的灵敏度主要取决于制造晶片本身。机电耦合系数大，灵敏度就高。', '对', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (29, '18d92be960c911edb68cf80dac0b7f41', 4, 2, '所有的传感器都必须将被测参量转换成为电压信号。', '错', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (30, 'a46bea76625211ed834cf80dac0b7f41', 3, 2, '用邻接矩阵法存储一个图所需的存储单元数目与图的边数有关()', '错', '用邻接矩阵法存储图,占用的存储空间数只与图中结点个数有关,而与边数无关。', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (31, 'a46c101d625211ed834cf80dac0b7f41', 3, 2, '二叉树的遍历只是为了在应用中找到一种线性次序。', '对', '所谓遍历二叉树，就是按一定的规则和顺序走遍二叉树的所有结点，使每一个结点都被访问一次，而且只被访问一次。由于二叉树是非线性结构，因此，树的遍历实质上是将二叉树的各个结点转换成为一个线性序列来表示。', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (32, 'a46c1eda625211ed834cf80dac0b7f41', 4, 2, '通常将一秒时间内声波传播的距离称作波长', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (33, 'a46c2e20625211ed834cf80dac0b7f41', 4, 2, '通常将频率高于20kHz的机械波称为超声波', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (34, 'ae6a98b4625211ed834cf80dac0b7f41', 4, 2, '在超声波发射电路设计中采用反相器构成驱动电路可以增大超声波发射探头的驱动电流。', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (35, 'ae6ab595625211ed834cf80dac0b7f41', 4, 2, '超声波只有40kHz一种频率信号', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (36, 'ae6ac1c7625211ed834cf80dac0b7f41', 4, 2, '超声波不仅可以直线传播，还可以进行绕射传播', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (37, 'ae6ad8c9625211ed834cf80dac0b7f41', 4, 2, '超声波传感器的灵敏度主要取决于制造晶片本身。机电耦合系数大，灵敏度就高。', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (38, 'b8133c67625211ed834cf80dac0b7f41', 4, 2, '在超声波测距电路中，如果超声波的频率不稳定，将会使测量结果产生误差', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (39, 'b8134b52625211ed834cf80dac0b7f41', 4, 2, '通常将频率高于20Hz的机械波称为超声波', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (40, 'b8135b1e625211ed834cf80dac0b7f41', 4, 2, '超声波接收头接收超声波是利用超声波的正压电效应', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (41, 'b813853b625211ed834cf80dac0b7f41', 4, 2, '超声波发射头发射超声波是利用超声波的正压电效应', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (42, 'b8139865625211ed834cf80dac0b7f41', 4, 2, '超声波传感器的灵敏度主要取决于制造晶片本身。机电耦合系数大，灵敏度就高。', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (43, 'bfb4d68c625211ed834cf80dac0b7f41', 4, 2, '金属应变片的工作原理是基于金属的压阻效应', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (44, 'bfb4e127625211ed834cf80dac0b7f41', 4, 2, '利用电感式传感器可以进行金属探测', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (45, 'bfb4f24a625211ed834cf80dac0b7f41', 4, 2, '力敏传感器工作时，可以利用弹性元件将被测压力、重量等参量直接转换成电量输出。', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (46, 'bfb519c8625211ed834cf80dac0b7f41', 4, 2, '半导体应变片的工作原理是基于半导体材料的压阻效应。\r\n\r\n', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (47, 'bfb52c44625211ed834cf80dac0b7f41', 4, 2, '金属应变片与半导体应变片相比具有更高的测量灵敏度', '错', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (48, 'c895083a625211ed834cf80dac0b7f41', 4, 2, '利用电容式传感器可以构成电阻产品的触摸按键', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (49, 'c89517a1625211ed834cf80dac0b7f41', 2, 2, 'If your sister looks after a sick person to make money, she is working in a hospital.', '对', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (50, 'c89531f1625211ed834cf80dac0b7f41', 2, 2, 'You are buying from the market when you do some window-shopping.', '错', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (51, 'c8954739625211ed834cf80dac0b7f41', 2, 2, 'The word \'\'real\'\' in the second sentence of the last paragraph most probably means \'\'important\'\'.', '错', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (52, 'c89554a4625211ed834cf80dac0b7f41', 2, 2, 'You will probably get the message about how to grow your potatoes when nobody buys your potatoes.', '错', '暂无解析', '中等', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (53, 'bb8844f2625511ed8f5bf80dac0b7f41', 2, 2, 'The best title for the passage would be \"What Is the Market\".', '对', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (54, 'bb886cea625511ed8f5bf80dac0b7f41', 2, 2, 'The Internet was not designed with security in mind.', '对', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (55, 'bb887e0a625511ed8f5bf80dac0b7f41', 2, 2, 'Sniffer can detect sensitive information in packet plaintext ', '对', '暂无解析', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (56, 'bb8897d5625511ed8f5bf80dac0b7f41', 2, 2, 'The application can run in the network core', '错', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (57, 'bb88b275625511ed8f5bf80dac0b7f41', 2, 2, 'In client-server structure, there is no need for direct communication  between clients', '对', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (58, 'c715012c625511ed8f5bf80dac0b7f41', 2, 2, 'In a P2P structure, each peer entity ACTS as both a client and a server.', '对', '暂无解析', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (59, 'c7151dee625511ed8f5bf80dac0b7f41', 2, 2, ' Identifying a process requires an IP address and host name', '错', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (60, 'c71536ca625511ed8f5bf80dac0b7f41', 2, 2, 'The process can send and receive information through the socket', '对', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (61, 'c7154aea625511ed8f5bf80dac0b7f41', 3, 2, '取线性表的第i个元素的时间同i的大小有关。', '错', '顺序表最主要的特点是随机访问，即通过首地址和元素序号可以在O(1)的时间内找到指定的元素\r\n线性表因为是按序号直接取值，所以没有关系，但如果是链式存储结构就有关系', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (62, 'c7156c83625511ed8f5bf80dac0b7f41', 3, 2, '在一个设有头指针和尾指针的单链表中,执行删除该单链表中最后一个元素的操作与链表的长度无关', '错', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (63, 'cea2b444625511ed8f5bf80dac0b7f41', 3, 2, '若采用“队首指针和队尾指针的值相等”作为环形队列为空的标志，则在设置一个空队时只需将队首指针和队尾指针赋同一个值，不管什么值都可以。', '对', '判断队满的方式一：牺牲一个存储的单元来区分空队、满队', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (64, 'cea2cc83625511ed8f5bf80dac0b7f41', 3, 2, '存在一棵总共有2016个结点的二叉树，其中有16个结点只有一个孩子', '错', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (65, 'cea2e446625511ed8f5bf80dac0b7f41', 3, 2, '具有10个叶结点的二叉树中，有9个度为2的结点。', '对', '度为0的节点称为叶节点\r\n叶结点个数=度为2的结点个数+1', '中等', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (66, 'cea2fde6625511ed8f5bf80dac0b7f41', 3, 2, '在含有n个结点的树中，边数只能是n-1条。', '对', '树中是不存在环的，对于有N个节点的树，必定是N-1条边 。', '简单', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (67, 'cea31b2b625511ed8f5bf80dac0b7f41', 3, 2, '完全二叉树中，若一个结点没有左孩子，则它必是树叶。', '对', '暂无解析', '困难', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (68, 'd692a6d6625511ed8f5bf80dac0b7f41', 3, 2, '一棵有124个结点的完全二叉树，其叶结点个数是确定的。', '对', '一棵124个叶结点的完全二叉树，假设n0为叶子结点数，n1为度为1结点数，n2为度为2结点数，则有总结点数为n0+n1+n2;而n2=n0-1=123；且完全二叉树中度为1的结点只能为一个或0个，所以总结点数为124+1+123=248个', '简单', '已审核', 'false');
INSERT INTO `trueorfalse` VALUES (69, 'd692bcf0625511ed8f5bf80dac0b7f41', 3, 2, '非空的二叉树一定满足：某结点若有左孩子，则其中序前驱一定没有右孩子', '对', '暂无解析', '中等', '未审核', 'false');
INSERT INTO `trueorfalse` VALUES (70, 'd692d03b625511ed8f5bf80dac0b7f41', 3, 2, '在有N个元素的最大堆中，随机访问任意键值的操作可以在O(logN)时间完成', '错', '堆的左右孩子没有固定的顺序，无法像平衡二叉树那样顺着找下去。', '简单', '未审核', 'false');

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `typeId` int(0) NOT NULL AUTO_INCREMENT,
  `typeName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`typeId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES (1, '选择题');
INSERT INTO `type` VALUES (2, '判断题');
INSERT INTO `type` VALUES (3, '简答题');

-- ----------------------------
-- Table structure for wechat_user
-- ----------------------------
DROP TABLE IF EXISTS `wechat_user`;
CREATE TABLE `wechat_user`  (
  `userId` int(0) NOT NULL AUTO_INCREMENT,
  `openId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userId`, `name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wechat_user
-- ----------------------------
INSERT INTO `wechat_user` VALUES (1, '1', '吴祁袁', '84623451@qq.com', '139279329');
INSERT INTO `wechat_user` VALUES (2, '2', '陈永发', '134567@qq.com', '13927959');
INSERT INTO `wechat_user` VALUES (3, '3', '李文乐', '1231568266', '139277891');
INSERT INTO `wechat_user` VALUES (4, '4', '钟志伟', '8723873@qq.com', '123827342');
INSERT INTO `wechat_user` VALUES (5, '5', '叶林', '384338@qq.com', '1834849');

-- ----------------------------
-- Triggers structure for table exam
-- ----------------------------
DROP TRIGGER IF EXISTS `tg_exam_del`;
delimiter ;;
CREATE TRIGGER `tg_exam_del` BEFORE DELETE ON `exam` FOR EACH ROW begin
delete from examdata
where examdata.examId = old.examId;
end
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
