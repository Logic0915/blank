package com.zbxj.wyz.mapper;

import com.zbxj.wyz.domain.ExamData;
import com.zbxj.wyz.domain.dto.ExamDataChoiceDto;
import com.zbxj.wyz.domain.dto.ExamDataEssayDto;
import com.zbxj.wyz.domain.dto.ExamDataTrueOrFalseDto;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ExamDataMapper {

    @Select("select choiceId,choice.uuId,choiceProblem as problem,choiceDifficulty as difficulty from examdata\n" +
            "inner join exam on examdata.examId = exam.examId\n" +
            "inner join choice on choice.uuId = examdata.uuId\n" +
            "where exam.examId = #{examId}")
    List<ExamDataChoiceDto> getExamDataChoice(String examId);

    @Select("select trueOrFalseId,trueorfalse.uuId,trueOrFalseProblem as problem,trueOrFalseDifficulty as difficulty from examdata\n" +
            "inner join exam on examdata.examId = exam.examId\n" +
            "inner join trueorfalse on trueorfalse.uuId = examdata.uuId\n" +
            "where exam.examId = #{examId}")
    List<ExamDataTrueOrFalseDto> getExamDataTrueOrFalse(String examId);

    @Select("select essayId,essay.uuId,essayProblem as problem,essayDifficulty as difficulty from examdata\n" +
            "inner join exam on examdata.examId = exam.examId\n" +
            "inner join essay on essay.uuId = examdata.uuId\n" +
            "where exam.examId = #{examId}")
    List<ExamDataEssayDto> getExamDataEssay(String examId);

    int addProbelmListToExam(Map<String,List<ExamData>> paramMap);

    @Delete("delete from examData where examId = #{examId} and uuId = #{uuId}")
    int deleteOneExamData(ExamData examData);

}
