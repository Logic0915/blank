package com.zbxj.wyz.domain.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WechatUserDto implements Serializable {

    private int userId;
    private String openId;
    private String name;
    private String mail;
    private String phone;
}
