package com.zbxj.wyz.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;


@TableName(value="choice")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Choice {
    @TableId
    private Integer choiceId ;
    private String uuId;
    private Integer courseId;
    private Integer typeId;

    private String choiceProblem;
    private String choiceOp1;
    private String choiceOp2;
    private String choiceOp3;
    private String choiceOp4;
    private String choiceAnsw;
    private String choiceParse;

    private String choiceDifficulty;
    private String choiceAudit;
    private String choiceDelete;

    //自定义的构造方法
    public Choice(Integer courseId, Integer typeId, String choiceProblem, String choiceOp1, String choiceOp2, String choiceOp3, String choiceOp4, String choiceAnsw, String choiceParse, String difficulty, String audit) {
        this.courseId = courseId;
        this.typeId = typeId;
        this.choiceProblem = choiceProblem;
        this.choiceOp1 = choiceOp1;
        this.choiceOp2 = choiceOp2;
        this.choiceOp3 = choiceOp3;
        this.choiceOp4 = choiceOp4;
        this.choiceAnsw = choiceAnsw;
        this.choiceParse = choiceParse;
        this.choiceDifficulty = difficulty;
        this.choiceAudit = audit;
    }

}
