package com.zbxj.wyz.service;


import com.zbxj.wyz.domain.Choice;
import com.zbxj.wyz.domain.Tag;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.domain.dto.TagDto;
import com.zbxj.wyz.domain.dto.TagToTreeDto;
import com.zbxj.wyz.mapper.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    @Autowired
    TagMapper tagMapper;

    public Result getAllTag(int courseId){
        List<TagDto> tagList = tagMapper.getAllTag(courseId);

        if (!tagList.isEmpty())
            return new Result().ok("获取标签列表成功",tagList);
        else
            return new Result().serverError("获取标签列表失败");
    }

    public Result deleteTag(Tag tag){
        if(tagMapper.deleteTag(tag) > 0)
            return new Result().ok("删除标签成功");
        else
            return new Result().serverError("删除标签失败");
    }

    public Result updateTag(Tag tag){
        if(tagMapper.updateTag(tag) > 0)
            return new Result().ok("修改标签成功");
        else
            return new Result().serverError("修改标签失败");
    }

    public Result addTag(Tag tag){
        if(tagMapper.addTag(tag) > 0)
            return new Result().ok("添加标签成功");
        else
            return new Result().serverError("添加标签失败");
    }

    public String getTagNameById(int tagId){
        return tagMapper.getTagNameById(tagId);
    }

    public Result auditTag(Tag tag){
        if(tagMapper.auditTag(tag) >0)
            return new Result<>().ok("审核标签成功");
        else
            return new Result().serverError("审核标签失败");
    }

    public Result getTagAuditList(int courseId){
        List<TagDto> tagAuditList = tagMapper.getTagAuditList(courseId);
        if (!tagAuditList.isEmpty())
            return new Result<>().ok("获取标签列表成功",tagAuditList);
        else
            return new Result().serverError("获取标签列表失败");
    }

    public Result getTagToTreeList(int courseId){
        List<TagToTreeDto> recursionTagList = tagMapper.getTagToTreeList(courseId);

        if (!recursionTagList.isEmpty())
            return new Result<>().ok("获取标签列表成功",recursionTagList);
        else
            return new Result().serverError("获取标签列表失败");
    }
}
