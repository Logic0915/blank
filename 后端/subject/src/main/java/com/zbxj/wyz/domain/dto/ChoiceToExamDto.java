package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChoiceToExamDto {
    private Integer choiceId;
    private String uuId;
    private String problem;
    private String choiceOp1;
    private String choiceOp2;
    private String choiceOp3;
    private String choiceOp4;
    private String choiceAnsw;
    private String choiceParse;
    private String difficulty;
}
