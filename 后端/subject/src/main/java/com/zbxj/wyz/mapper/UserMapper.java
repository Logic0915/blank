package com.zbxj.wyz.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbxj.wyz.domain.SystemUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper extends BaseMapper<SystemUser> {
    @Insert("insert into t_user (name,pwd,state,authority,phone,mail)values (#{name},#{pwd},1,'ROLE_common',#{phone},#{mail})")
    int addUser(SystemUser user);

    @Select("select * from t_user where name=#{name}")
    SystemUser getnameUser(String name);
}
