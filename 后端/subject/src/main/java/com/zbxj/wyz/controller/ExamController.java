package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.Exam;

import com.zbxj.wyz.domain.dto.ExamDto;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.service.CourseService;
import com.zbxj.wyz.service.ExamDataService;
import com.zbxj.wyz.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@ResponseBody
public class ExamController {

    @Autowired
    CourseService courseService;

    @Autowired
    ExamService examService;

    @Autowired
    ExamDataService examDataService;

    @GetMapping("/exam/{courseName}")
    public ResponseEntity getExamByCourseName(@PathVariable("courseName") String courseName){
        return ResponseEntity.ok(examService.getExamByCourseName(courseName));
    }

    @PostMapping("/exam/add")
    public ResponseEntity addExamAndProblem(@RequestBody ExamDto examDto){
        return ResponseEntity.ok(examService.addExamAndProblem(examDto));
    }

    @PostMapping("/exam/delete")
    public ResponseEntity deleteExam(@RequestBody Exam exam){
        return ResponseEntity.ok(examService.deleteExam(exam));
    }

    @PostMapping("/exam/update")
    public ResponseEntity updateExam(@RequestBody Exam exam){
        return ResponseEntity.ok(examService.updateExam(exam));
    }
}
