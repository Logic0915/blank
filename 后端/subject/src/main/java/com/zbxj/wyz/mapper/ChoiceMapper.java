package com.zbxj.wyz.mapper;



import com.zbxj.wyz.domain.Choice;
import com.zbxj.wyz.domain.dto.ChoiceDto;
import com.zbxj.wyz.domain.dto.ChoiceToExamDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ChoiceMapper {
    //查           根据条件查
    List<ChoiceDto> getChoiceByCondition(String courseName, String choiceDifficulty, String choiceAudit);

    //增
    @Insert("insert into choice" +
            "(courseId,typeId,choiceProblem,choiceOp1,choiceOp2,choiceOp3,choiceOp4,\n" +
            "choiceAnsw,choiceParse,choiceDifficulty,choiceAudit)\n" +
            "values " +
            "(#{courseId},#{typeId},#{choiceProblem},#{choiceOp1},#{choiceOp2},#{choiceOp3},#{choiceOp4},\n" +
            "#{choiceAnsw},#{choiceParse},#{choiceDifficulty},'未审核')")
    int addChoice(Choice choice);

    //删
    @Update("update choice set choiceDelete = 'true' " +
            "where choiceId=#{choiceId}")
    int deleteChoice(Choice choice);

    //改
    @Update("update choice\n" +
            "set choiceProblem=#{choiceProblem},\n" +
            "choiceOp1=#{choiceOp1},choiceOp2=#{choiceOp2}," +
            "choiceOp3=#{choiceOp3},choiceOp4=#{choiceOp4},\n" +
            "choiceAnsw=#{choiceAnsw},choiceParse=#{choiceParse},\n" +
            "choiceDifficulty=#{choiceDifficulty}\n" +
//            ",choiceAudit='false'\n" +
            "where choiceId=#{choiceId}")
    int updateChoice(Choice choice);

    @Update("update choice set choiceAudit = '已审核' where choiceId = #{choiceId}")
    int auditChoice(Choice choice);

    List<ChoiceToExamDto> getAllChoiceToExam(String courseName,String choiceDifficulty);

}
