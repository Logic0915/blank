package com.zbxj.wyz.domain.dto;

import com.zbxj.wyz.domain.TrueOrFalse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrueOrFalseDto {
    private Integer trueOrFalseId;
    private String courseName;
    private String typeName;

    private String problem;
    private String difficulty;
    private String audit;

    private String trueOrFalseProblem;
    private String trueOrFalseAnsw;
    private String trueOrFalseParse;

    private String trueOrFalseDifficulty;
    private String trueOrFalseAudit;

    //将ChioceDto对象转换为Chioce对象
    public TrueOrFalse toTrueOrFalse(int courseId, int typeId, TrueOrFalseDto trueOrFalseDto){
        return new TrueOrFalse(
                courseId,typeId, trueOrFalseDto.getTrueOrFalseProblem(), trueOrFalseDto.getTrueOrFalseAnsw(),
                trueOrFalseDto.getTrueOrFalseParse(), trueOrFalseDto.getTrueOrFalseDifficulty(),trueOrFalseDto.getTrueOrFalseAudit()
        );
    }
}
