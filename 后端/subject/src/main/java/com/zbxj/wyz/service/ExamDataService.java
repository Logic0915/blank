package com.zbxj.wyz.service;

import com.zbxj.wyz.domain.ExamData;
import com.zbxj.wyz.domain.dto.*;
import com.zbxj.wyz.mapper.ExamDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExamDataService {
//    List<ExamDataChoiceDto> getExamDataByChoice(String examId);

    @Autowired
    ExamDataMapper examDataMapper;

    public Result getExamDataChoice(String examId){
        List<ExamDataChoiceDto> examDataChoiceDtos = examDataMapper.getExamDataChoice(examId);
        if (!examDataChoiceDtos.isEmpty())
            return new Result<List<ExamDataChoiceDto>>().ok("获取试卷列表成功",examDataChoiceDtos);
        else
            return new Result().serverError("获取试卷列表失败");
    }

    public Result getExamDataTrueOrFalse(String examId){
        List<ExamDataTrueOrFalseDto> examDataTrueOrFalseDtos = examDataMapper.getExamDataTrueOrFalse(examId);
        if (!examDataTrueOrFalseDtos.isEmpty())
            return new Result<List<ExamDataTrueOrFalseDto>>().ok("获取试卷列表成功",examDataTrueOrFalseDtos);
        else
            return new Result().serverError("获取试卷列表失败");
    }

    public Result getExamDataEssay(String examId){
        List<ExamDataEssayDto> examDataEssayDtos = examDataMapper.getExamDataEssay(examId);
        if (!examDataEssayDtos.isEmpty())
            return new Result<List<ExamDataEssayDto>>().ok("获取试卷列表成功",examDataEssayDtos);
        else
            return new Result().serverError("获取试卷列表失败");
    }


    public Result addProbelmListToExam(Integer examId, List<String> uuIdList){
        //处理数据参数，以批量向数据库添加数据
        List<ExamData> paramList = new ArrayList<>();
        for(String uuId : uuIdList){
            ExamData examData = new ExamData(examId,uuId);
            paramList.add(examData);
        }

        //参数封装为HashMap
        Map<String,List<ExamData>> paramMap = new HashMap();
        paramMap.put("paramList",paramList);

        //添加到数据库
        if(examDataMapper.addProbelmListToExam(paramMap) > 0)
            return new Result().ok("批量向数据添加数据成功");
        else
            return new Result().serverError("批量向数据添加数据失败");
    }

    public Result deleteOneExamData(ExamData examData){
        if(examDataMapper.deleteOneExamData(examData) > 0)
            return new Result().ok("删除试卷中的一个题目成功");
        else
            return new Result().serverError("删除试卷中的一个题目失败");
    }
}
