package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * 功能描述：
 *
 * @Auther: 梁展鹏
 * @Date: 2018/7/19 09:49
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
public class Result<T> implements Serializable {


    private Integer code;

    private String message;

    private T data;

    public Result ok() {
        return new Result(HttpStatus.OK.value(), "操作成功", null);
    }

    public Result ok(String message) {
        return new Result(HttpStatus.OK.value(), message, null);
    }

    public Result ok(T data) {
        return new Result(HttpStatus.OK.value(), "", data);
    }

    public Result ok(String message, T data) {
        return new Result(HttpStatus.OK.value(), message, data);
    }

    public Result serverError() {
        return new Result(HttpStatus.INTERNAL_SERVER_ERROR.value(), "服务器异常", null);
    }

    public Result serverError(String message) {
        return new Result(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
    }

    public Result serverError(T data) {
        return new Result(HttpStatus.INTERNAL_SERVER_ERROR.value(), "", data);
    }

    public Result serverError(String message, T data) {
        return new Result(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, data);
    }

}
