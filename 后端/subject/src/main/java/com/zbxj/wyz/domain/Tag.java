package com.zbxj.wyz.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@TableName(value="tag")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tag {
    @TableId
    private Integer tagId;
    private String tagName;
    private Integer parentTagId;
    private Integer courseId;
    private String tagColour;
    private String tagAudit;
}
