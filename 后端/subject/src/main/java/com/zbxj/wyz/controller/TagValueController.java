package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.Exam;
import com.zbxj.wyz.domain.Tag;
import com.zbxj.wyz.domain.TagValue;
import com.zbxj.wyz.service.TagValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@ResponseBody
public class TagValueController {

    @Autowired
    TagValueService tagValueService;

    @GetMapping("/tagvalue/{tagId}")
    public ResponseEntity getTagValueByTagId(@PathVariable("tagId") int tagId){
        return ResponseEntity.ok(tagValueService.getTagValueByTagId(tagId));
    }

    @PostMapping("/tagvalue/add")
    public ResponseEntity addTagValue(@RequestBody TagValue tagValue){
        return ResponseEntity.ok(tagValueService.addTagValue(tagValue));
    }

    @PostMapping("/tagvalue/delete")
    public ResponseEntity deleteTagValue(@RequestBody TagValue tagValue){
        return ResponseEntity.ok(tagValueService.deleteTagValue(tagValue));
    }

    @PostMapping("/tagvalue/update")
    public ResponseEntity updateTagValue(@RequestBody TagValue tagValue){
        return ResponseEntity.ok(tagValueService.updateTagValue(tagValue));
    }

    @PostMapping("/tagvalue/audit")
    public ResponseEntity auditTagValue(@RequestBody TagValue tagValue){
        return ResponseEntity.ok(tagValueService.auditTagValue(tagValue));
    }

    @GetMapping("/tagvalue/auditList/{tagId}")
    public ResponseEntity getTagValueAuditList(@PathVariable("tagId") int tagId){
        return ResponseEntity.ok(tagValueService.getTagValueAuditList(tagId));
    }
}
