package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagDto {
    private Integer tagId;
    private String tagName;
    private Integer parentTagId;
    private String parentTagName;
    private String tagColour;
    private String tagAudit;
}
