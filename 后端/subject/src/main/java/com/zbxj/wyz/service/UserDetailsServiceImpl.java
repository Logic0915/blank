package com.zbxj.wyz.service;


import com.zbxj.wyz.domain.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//UserDetailsService是spring Security提供的用于封装认证用户信息的接口
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserService userService;

    /**
     * 该方法的作用是查询数据库中用户为name的用户信息，将数据库查询到的用户名、用户密码和用户权限封装在UserDetails中。
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 通过自定义的方法获取用户及权限信息
        SystemUser user = userService.getUser(username);
        // 对用户权限进行封装，将SystemUser对象中的authorities字段转换成List<SimpleGrantedAuthority类型
        List<SimpleGrantedAuthority> list = new ArrayList<SimpleGrantedAuthority>();
        list.add(new SimpleGrantedAuthority(user.getAuthority()));

        if (user != null) {
            //User类是框架提供的类，是UserDetails接口的实现类，用来存放用户名、用户密码、用户权限等信息。
            UserDetails userDetails = new User(user.getName(), user.getPwd(), list);
            return userDetails; // 返回封装的UserDetails用户详情类
        } else {
            // 如果查询的用户不存在（用户名不存在），必须抛出此异常
            throw new UsernameNotFoundException("当前用户不存在！");
        }
    }
}