package com.zbxj.wyz.service;
import com.zbxj.wyz.domain.Choice;
import com.zbxj.wyz.domain.dto.ChoiceDto;
import com.zbxj.wyz.domain.dto.ChoiceToExamDto;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.mapper.ChoiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class ChoiceService {

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    ChoiceMapper choiceMapper;

    //查:提供给SelectAllService的getQuestionByCondition使用
    @Cacheable(cacheNames="ChoiceByCondition")
    public List<ChoiceDto> getChoiceByCondition(String courseName, String choiceDifficulty, String choiceAudit){
        return choiceMapper.getChoiceByCondition(courseName, choiceDifficulty, choiceAudit);
    }

    //增
    public Result addChoice(Choice choice){
        if (choiceMapper.addChoice(choice) > 0)
            return new Result().ok("增加选择题成功");
        else
            return new Result().serverError("增加选择题失败");
    }
    //删
    @CacheEvict(cacheNames="ChoiceByCondition")
    public Result deleteChoice(Choice choice){
        //删除redis中所有以ChoiceByCondition开头的key,目的是刷新缓存
        Set<String> keys = redisTemplate.keys("ChoiceByCondition*");
        if (!CollectionUtils.isEmpty(keys)) {
            redisTemplate.delete(keys);
        }

        //更新数据库
        if(choiceMapper.deleteChoice(choice) > 0)
            return new Result().ok("软删除选择题成功");
        else
            return new Result().serverError("软删除选择题失败");
    }

    //改
    public Result updateChoice(Choice  choice){
        if(choiceMapper.updateChoice(choice) > 0)
            return new Result<>().ok("更新选择题成功");
        else
            return new Result().serverError("更新选择题失败");
    }

    public Result auditChoice(Choice choice){
        if(choiceMapper.auditChoice(choice) >0)
            return new Result<>().ok("审核选择题成功");
        else
            return new Result().serverError("审核选择题失败");
    }

    public Result getAllChoiceToExam(String courseName,String choiceDifficulty){
        List<ChoiceToExamDto> choiceList = choiceMapper.getAllChoiceToExam(courseName, choiceDifficulty);
        if(!choiceList.isEmpty())
            return new Result<List<ChoiceToExamDto>>().ok("获取选择题列表（试卷用途）成功",choiceList);
        else
            return new Result().serverError("获取选择题列表（试卷用途）失败");
    }

}
