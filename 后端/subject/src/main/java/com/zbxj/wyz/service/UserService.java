package com.zbxj.wyz.service;


import com.zbxj.wyz.domain.SystemUser;
import com.zbxj.wyz.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

//在UserDetailsServiceImpl类中需要用到UserService，UserService是自定义的类，该类的主要功能是获取用户为name的用户对象和用户权限，并把获取到的信息存放在缓存中。
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    RedisTemplate redisTemplate;
    //根据name获取对应的SystemUser对象，该SystemUser对象包含了用户名、密码和权限。如果一个用户有多个权限，最好在本类中再定义一个独立的方法获取用户权限。由于本例中一个用户只对应一个权限，所以只用一个方法获取用户名、密码和权限
    public SystemUser getUser(String name){
        SystemUser user = null;
        //判断缓存中是否存在键为“user_XXX"的缓存对象
        Object o= redisTemplate.opsForValue().get("user_"+name);
        if(o!=null){
            user=(SystemUser)o;
        }else {
            user = userMapper.selectById(name);
            if(user!=null){
                //将数据库查询到的SystemUser对象存放在缓存中，键名为"user_XXX"
                redisTemplate.opsForValue().set("user_"+name,user);
            }
        }
        return user;
    }



//    //根据name获取该用户的权限，如果一个用户有多个权限，最好保留该方法。由于本例中一个用户只对应一个权限，所以可以删掉该方法
//    public List getAuthority(String name) {
//        return null;
//    }
}
