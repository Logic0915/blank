package com.zbxj.wyz.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TypeMapper {

    @Select("select typeId from type where typeName=#{typeName}")
    int getIdByTypeName(String typeName);

}
