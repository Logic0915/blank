package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExamDataTrueOrFalseDto {
    private Integer trueOrFalseId;
    private String uuId;
    private String problem;
    private String difficulty;
}
