package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExamDataChoiceDto {
//    private Integer examId;
    private Integer choiceId;
    private String uuId;
    private String problem;
    private String difficulty;
}
