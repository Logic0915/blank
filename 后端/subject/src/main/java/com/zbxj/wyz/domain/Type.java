package com.zbxj.wyz.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@TableName(value="type")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Type {
    @TableId
    private  Integer typeId;
    private String typeName;
}
