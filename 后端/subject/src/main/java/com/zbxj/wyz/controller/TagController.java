package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.Choice;
import com.zbxj.wyz.domain.Tag;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@ResponseBody
public class TagController {
    @Autowired
    TagService tagService;

    @GetMapping("/tag/{courseId}")
    public ResponseEntity getAllTag(@PathVariable("courseId") int courseId){
        return ResponseEntity.ok(tagService.getAllTag(courseId));
    }

    @PostMapping("/tag/delete")
    public ResponseEntity deleteTag(@RequestBody Tag tag){
        return ResponseEntity.ok(tagService.deleteTag(tag));
    }

    @PostMapping("/tag/update")
    public ResponseEntity updateTag(@RequestBody Tag tag){
        return ResponseEntity.ok(tagService.updateTag(tag));
    }

    @PostMapping("/tag/add")
    public ResponseEntity addTag(@RequestBody Tag tag){
        return ResponseEntity.ok(tagService.addTag(tag));
    }

    @PostMapping("/tag/audit")
    public ResponseEntity auditTag(@RequestBody Tag tag){
        return ResponseEntity.ok(tagService.auditTag(tag));
    }

    @GetMapping("/tag/auditList/{courseId}")
    public ResponseEntity getTagAuditList(@PathVariable("courseId") int courseId){
        return ResponseEntity.ok(tagService.getTagAuditList(courseId));
    }

    @GetMapping("/tag/toTreeList/{courseId}")
    public ResponseEntity getTagToTreeList(@PathVariable("courseId") int courseId){
        return ResponseEntity.ok(tagService.getTagToTreeList(courseId));
    }
}
