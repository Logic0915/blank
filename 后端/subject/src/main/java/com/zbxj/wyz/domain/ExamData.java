package com.zbxj.wyz.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@TableName(value="examdata")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExamData {
    @TableId
    private Integer examDataId;
    private Integer examId;
    private String uuId;

    public ExamData(Integer examId, String uuId) {
        this.examId = examId;
        this.uuId = uuId;
    }
}
