package com.zbxj.wyz.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@TableName(value="Essay")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Essay {
    @TableId
    private  Integer essayId;
    private  Integer courseId;
    private  Integer typeId;

    private String essayProblem;
    private String essayAnsw;
    private String essayParse;

    private String essayDifficulty;
    private String essayAudit;
    private String essayDelete;

    public Essay(int courseId, int typeId, String essayProblem, String essayAnsw, String essayParse, String essayDifficulty, String essayAudit) {
        this.courseId = courseId;
        this.typeId = typeId;
        this.essayProblem = essayProblem;
        this.essayAnsw = essayAnsw;
        this.essayParse = essayParse;
        this.essayDifficulty = essayDifficulty;
        this.essayAudit = essayAudit;

    }
}
