package com.zbxj.wyz.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zbxj.wyz.domain.Choice;
import com.zbxj.wyz.domain.Exam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExamDto {
    private Integer examId;
    private String examName;
    private String courseName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime examDate;
    private Integer totalTime;
    private Integer totalScore;
    private String state;
    private String examNote;

    private List<String> uuIdList;

    //将ExamDto对象转换为Exam对象
    public Exam toExam(int courseId, ExamDto examDto){
        return new Exam(
                examDto.getExamId(),examDto.getExamName(),courseId,examDto.getExamDate(),
                examDto.getTotalTime(),examDto.getTotalScore(),examDto.getState(),examDto.getExamNote()
        );
    }
}
