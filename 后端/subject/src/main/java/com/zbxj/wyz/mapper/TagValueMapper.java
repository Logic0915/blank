package com.zbxj.wyz.mapper;

import com.zbxj.wyz.domain.TagValue;
import com.zbxj.wyz.domain.dto.TagDto;
import com.zbxj.wyz.domain.dto.TagValueDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TagValueMapper {

    @Select("select tagValueId,tagValue,tagValueAudit from tagValue where tagId=#{tagId} and tagValueAudit = '已审核'")
    List<TagValueDto> getTagValueByTagId(int tagId);

    @Delete("delete from tagValue where tagValueId = #{tagValueId}")
    int deleteTagValue(TagValue tagValue);

    @Update("update tagValue set tagValue = #{tagValue} where tagValueId = #{tagValueId}")
    int updateTagValue(TagValue tagValue);

    @Insert("insert into tagValue (tagId,tagValue) values (#{tagId},#{tagValue})")
    int addTagValue(TagValue tagValue);

    @Update("update tagValue set tagValueAudit = '已审核' where tagValueId = #{tagValueId}")
    int auditTagValue(TagValue tagValue);


    @Select("select tagValueId,tagValue,tagName,tagValueAudit from tagValue\n" +
            "inner join tag on tag.tagId = tagvalue.tagId\n" +
            "where tagValueAudit = '未审核' and tag.tagId = #{tagId}")
    List<TagValueDto> getTagValueAuditList(int tagId);
}
