package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EssayToExamDto {
    private Integer essayId;
    private String uuId;
    private String problem;
    private String essayAnsw;
    private String essayParse;
    private String difficulty;
}
