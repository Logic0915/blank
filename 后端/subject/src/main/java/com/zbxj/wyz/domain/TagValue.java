package com.zbxj.wyz.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@TableName(value="tagvalue")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagValue {
    @TableId
    private Integer tagValueId;
    private Integer tagId;
    private String tagValue;
    private String tagValueAudit;
}
