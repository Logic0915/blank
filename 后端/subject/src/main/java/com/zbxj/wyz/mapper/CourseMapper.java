package com.zbxj.wyz.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CourseMapper {
    @Select("select courseId from course where courseName=#{courseName}")
    int getIdByCourseName(String courseName);
}
