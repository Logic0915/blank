package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.TrueOrFalse;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.domain.dto.TrueOrFalseDto;
import com.zbxj.wyz.service.CourseService;
import com.zbxj.wyz.service.TrueOrFalseService;
import com.zbxj.wyz.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@ResponseBody
public class TrueOrFalseController {
    @Autowired
    TrueOrFalseService trueOrFalseService;
    @Autowired
    CourseService courseService;
    @Autowired
    TypeService typeService;
    @PostMapping("/trueOrFalse/update")
    public ResponseEntity updatetrueOrFalse(@RequestBody TrueOrFalse trueOrFalse){
        return ResponseEntity.ok(trueOrFalseService.updateTrueOrFalse(trueOrFalse));
    }
    @PostMapping("/trueOrFalse/audit")
    public ResponseEntity audittrueOrFalse(@RequestBody TrueOrFalse trueOrFalse){
        return ResponseEntity.ok(trueOrFalseService.auditTrueOrFalse(trueOrFalse));
    }
    @PostMapping("/trueOrFalse/add")
    public ResponseEntity addTrueOrFalse(@RequestBody TrueOrFalseDto trueOrFalseDto){
        int courseId = courseService.getIdByCourseName(trueOrFalseDto.getCourseName());
        int typeId = typeService.getIdByTypeName(trueOrFalseDto.getTypeName());
        TrueOrFalse trueOrFalse = trueOrFalseDto.toTrueOrFalse(courseId,typeId, trueOrFalseDto);
        return ResponseEntity.ok(trueOrFalseService.addTrueOrFalse(trueOrFalse));
    }

    @PostMapping("/trueOrFalse/delete")
    public ResponseEntity deleteTrueOrFalse(@RequestBody TrueOrFalse trueOrFalse){
        return ResponseEntity.ok(trueOrFalseService.deleteTrueOrFalse(trueOrFalse));
    }

    @GetMapping("/trueOrFalse/toexam/{courseName}/{trueOrFalseDifficulty}")
    public ResponseEntity getAllTrueOrFalseToExam(@PathVariable("courseName") String courseName, @PathVariable("trueOrFalseDifficulty") String trueOrFalseDifficulty){
        return ResponseEntity.ok(trueOrFalseService.getAllTrueOrFalseToExam(courseName,trueOrFalseDifficulty));
    }

}
