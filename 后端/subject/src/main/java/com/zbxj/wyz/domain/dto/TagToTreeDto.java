package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagToTreeDto {
    private Integer tagId;
    private String tagName;
    private Integer parentTagId;
    private String tagColour;
}
