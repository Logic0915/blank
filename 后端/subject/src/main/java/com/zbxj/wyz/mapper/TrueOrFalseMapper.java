package com.zbxj.wyz.mapper;




import com.zbxj.wyz.domain.TrueOrFalse;
import com.zbxj.wyz.domain.dto.ChoiceToExamDto;
import com.zbxj.wyz.domain.dto.TrueOrFalseDto;
import com.zbxj.wyz.domain.dto.TrueOrFalseToExamDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TrueOrFalseMapper {
    //查
    List<TrueOrFalseDto> getTrueOrFalseByCondition(String courseName, String trueOrFalseDifficulty, String trueOrFalseAudit);
    //改
    @Update("UPDATE trueorfalse set trueOrFalseProblem=#{trueOrFalseProblem},\n" +
            "trueOrFalseAnsw =#{trueOrFalseAnsw},\n" +
            "trueOrFalseParse=#{trueOrFalseParse},\n" +
            "trueOrFalseDifficulty=#{trueOrFalseDifficulty}\n" +
            "where trueOrFalseId=#{trueOrFalseId}")
    int updateTrueOrFalse (TrueOrFalse trueOrFalse);

    //审核
    @Update("update trueorfalse set trueOrFalseAudit='已审核' where trueOrFalseId =#{trueOrFalseId} ")
    int auditTrueOrFalse(TrueOrFalse trueOrFalse);

    @Insert("insert into trueorfalse" +
            "(courseId,typeId,trueOrFalseProblem,trueOrFalseAnsw,trueOrFalseParse,trueOrFalseDifficulty,trueOrFalseAudit)"+
            "values" +
            "(#{courseId},#{typeId},#{trueOrFalseProblem},#{trueOrFalseAnsw},#{trueOrFalseParse},#{trueOrFalseDifficulty},'未审核')")
    int addTrueOrFalse(TrueOrFalse trueOrFalse);

    //删
    @Update("update trueorfalse set trueOrFalseDelete = 'true' " +
            "where trueOrFalseId=#{trueOrFalseId}")
    int deleteTrueorfalse(TrueOrFalse trueOrFalse);

    List<TrueOrFalseToExamDto> getAllTrueOrFalseToExam(String courseName, String trueOrFalseDifficulty);
}
