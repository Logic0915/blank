package com.zbxj.wyz.domain.dto;

import com.zbxj.wyz.domain.Essay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EssayDto {
    private Integer essayId;
    private String courseName;
    private String typeName;

    private String problem;
    private String difficulty;
    private String audit;

    private String essayProblem;
    private String essayAnsw;
    private String essayParse;

    private String essayDifficulty;
    private String essayAudit;

    public Essay toEssay(int courseId, int typeId, EssayDto essayDto){
        return new Essay(
                courseId,typeId, essayDto.getEssayProblem(), essayDto.getEssayAnsw(),
                essayDto.getEssayParse(), essayDto.getEssayDifficulty(), essayDto.getEssayAudit()

        );
    }
}
