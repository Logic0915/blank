package com.zbxj.wyz.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@TableName(value="course")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course {
    @TableId
    private  Integer courseId;
    private String courseName;

    //某科目的选择题，查询某科目的选择题需要使用
    private Choice CourseForChoice;

    //某科目的题型，查询某科目的选择题需要使用
    private Type CourseForType;
}
