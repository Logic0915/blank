package com.zbxj.wyz.service;

import com.zbxj.wyz.mapper.CourseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {
    @Autowired
    CourseMapper courseMapper;

    public int getIdByCourseName(String courseName){
        return courseMapper.getIdByCourseName(courseName);
    }
}
