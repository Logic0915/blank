package com.zbxj.wyz.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("wechat_user")
public class WechatUser {
    @TableId
    private int userId;
    private String openId;
    private String name;
    private String mail;
    private String phone;
}
