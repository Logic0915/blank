package com.zbxj.wyz.service;

import com.zbxj.wyz.mapper.TypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeService {
    @Autowired
    TypeMapper typeMapper;

    public int getIdByTypeName(String typeName){
        return typeMapper.getIdByTypeName(typeName);
    }

}
