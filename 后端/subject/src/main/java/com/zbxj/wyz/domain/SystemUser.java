package com.zbxj.wyz.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

//因为在UserDetailsServiceImpl类中要用到org.springframework.security.core.userdetails.User，所以这里最好不要用User作为类名，以免混淆
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_user")
public class SystemUser implements Serializable {
    @TableId
    private String name;
    private String pwd;
    private int state;
    private String authority;
    private String phone;
    private String mail;
}

