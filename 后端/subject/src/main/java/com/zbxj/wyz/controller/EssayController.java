package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.Essay;
import com.zbxj.wyz.domain.dto.EssayDto;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.service.CourseService;
import com.zbxj.wyz.service.EssayService;
import com.zbxj.wyz.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@ResponseBody
//@RequestMapping(value = "/api")
public class EssayController {
    @Autowired
    EssayService essayService;

    @Autowired
    CourseService courseService;

    @Autowired
    TypeService typeService;

    @PostMapping("/essay/add")
    public ResponseEntity addEssay(@RequestBody EssayDto essayDto){
        int courseId = courseService.getIdByCourseName(essayDto.getCourseName());
        int typeId = typeService.getIdByTypeName(essayDto.getTypeName());
        Essay essay = essayDto.toEssay(courseId,typeId, essayDto);
        return ResponseEntity.ok(essayService.addEssay(essay));
    }

    @PostMapping("/essay/delete")
    public ResponseEntity deleteEssay(@RequestBody Essay essay){
        return ResponseEntity.ok(essayService.deleteEssay(essay));
    }

    @PostMapping("/essay/update")
    public  ResponseEntity updateEssay(@RequestBody Essay essay){
        return ResponseEntity.ok(essayService.updateEssay(essay));
    }

    @PostMapping("/essay/audit")
    public ResponseEntity auditEssay(@RequestBody Essay essay){
        return ResponseEntity.ok(essayService.auditEssay(essay));
    }

    @GetMapping("/essay/toexam/{courseName}/{essayDifficulty}")
    public ResponseEntity getAllEssayToExam(@PathVariable("courseName") String courseName, @PathVariable("essayDifficulty") String essayDifficulty){
        return ResponseEntity.ok(essayService.getAllEssayToExam(courseName,essayDifficulty));
    }
}
