package com.zbxj.wyz.service;

import com.zbxj.wyz.domain.TagValue;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.domain.dto.TagDto;
import com.zbxj.wyz.domain.dto.TagValueDto;
import com.zbxj.wyz.mapper.TagValueMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagValueService {

    @Autowired
    TagValueMapper tagValueMapper;

    public Result getTagValueByTagId(int tagId){
        List<TagValueDto> tagValueList = tagValueMapper.getTagValueByTagId(tagId);
        if (!tagValueList.isEmpty())
            return new Result().ok("获取标签值列表成功",tagValueList);
        else
            return new Result().serverError("获取标签值列表失败");
    }

    public Result deleteTagValue(TagValue tagValue){
        if(tagValueMapper.deleteTagValue(tagValue) > 0)
            return new Result().ok("删除标签值成功");
        else
            return new Result().serverError("删除标签值失败");
    }

    public Result updateTagValue(TagValue tagValue){
        if(tagValueMapper.updateTagValue(tagValue) > 0)
            return new Result().ok("修改标签值成功");
        else
            return new Result().serverError("修改标签值失败");
    }

    public Result addTagValue(TagValue tagValue){
        if(tagValueMapper.addTagValue(tagValue) > 0)
            return new Result().ok("添加标签值成功");
        else
            return new Result().serverError("添加标签值失败");
    }

    public Result auditTagValue(TagValue tagValue){
        if(tagValueMapper.auditTagValue(tagValue) >0)
            return new Result<>().ok("审核标签值成功");
        else
            return new Result().serverError("审核标签值失败");
    }

    public Result getTagValueAuditList(int tagId){
        List<TagValueDto> tagValueAuditList = tagValueMapper.getTagValueAuditList(tagId);
        if (!tagValueAuditList.isEmpty())
            return new Result<>().ok("获取标签值列表成功",tagValueAuditList);
        else
            return new Result().serverError("获取标签值列表失败");
    }
}
