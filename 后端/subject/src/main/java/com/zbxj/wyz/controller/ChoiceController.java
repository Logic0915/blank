package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.Choice;
import com.zbxj.wyz.domain.dto.ChoiceDto;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.service.ChoiceService;
import com.zbxj.wyz.service.CourseService;
import com.zbxj.wyz.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@ResponseBody
public class ChoiceController {

    @Autowired
    ChoiceService choiceService;

    @Autowired
    CourseService courseService;

    @Autowired
    TypeService typeService;

    /**
     * 增
     * 数据输入案例：
     * {
     *     "courseName":"数学",
     *     "typeName":"选择题",
     *     "choiceProblem":"题目",
     *     "choiceOp1":"选项1",
     *     "choiceOp2":"选项2",
     *     "choiceOp3":"选项3",
     *     "choiceOp4":"选项4",
     *     "choiceAnsw":"a",
     *     "choiceParse":"解析"
     *     "choiceDifficulty":1
     *     "choiceAudit":"t"
     * }
     * @param choiceDto 数据传输对象
     * @return  Result
     */
    @PostMapping("/choice/add")
    public ResponseEntity addChoice(@RequestBody ChoiceDto choiceDto){
        int courseId = courseService.getIdByCourseName(choiceDto.getCourseName());
        int typeId = typeService.getIdByTypeName(choiceDto.getTypeName());
        Choice choice = choiceDto.toChioce(courseId,typeId, choiceDto);
        return ResponseEntity.ok(choiceService.addChoice(choice));
    }

    /**
     * 删
     * 数据输入案例：
     * {
     *     "choiceId":3
     * }
     * @param choice    通过choice获取里面的choiceId
     * @return Result
     */
    @PostMapping("/choice/delete")
    public ResponseEntity deleteChoice(@RequestBody Choice choice){
        return ResponseEntity.ok(choiceService.deleteChoice(choice));
    }

    /**
     * 改
     * 数据输入案例：
     * {
     *     "choiceId":20,
     *     "choiceProblem":"题目1232",
     *     "choiceOp1":"213",
     *     "choiceOp2":"213213",
     *     "choiceOp3":"213123",
     *     "choiceOp4":"sdsad123",
     *     "choiceAnsw":"a",
     *     "choiceParse":"解析9999",
     *     "choiceDifficulty":3,
     *     "choiceAudit":"t"
     * }
     * 并非所有choice的数据项都可以修改，其中科目、题型、软删除标识符不允许修改，其他数据项可修改。
     * @param choice    此类是为了获取更新的数据
     * @return Result
     */
    @PostMapping("/choice/update")
    public ResponseEntity updateChoice(@RequestBody Choice choice){
        return ResponseEntity.ok(choiceService.updateChoice(choice));
    }

    // 审核选择题
    @PostMapping("/choice/audit")
    public ResponseEntity auditChoice(@RequestBody Choice choice){
        return ResponseEntity.ok(choiceService.auditChoice(choice));
    }

    @GetMapping("/choice/toexam/{courseName}/{choiceDifficulty}")
    public ResponseEntity getAllChoiceToExam(@PathVariable("courseName") String courseName,@PathVariable("choiceDifficulty") String choiceDifficulty){
        return ResponseEntity.ok(choiceService.getAllChoiceToExam(courseName,choiceDifficulty));
    }
}
