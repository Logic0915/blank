package com.zbxj.wyz.service;


import com.zbxj.wyz.domain.WechatUser;
import com.zbxj.wyz.domain.dto.ChoiceDto;
import com.zbxj.wyz.domain.dto.ChoiceToExamDto;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.domain.dto.WechatUserDto;
import com.zbxj.wyz.mapper.WechatUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WechatUserService {


    @Autowired
    WechatUserMapper wechatUserMapper;

    @Autowired
    RedisTemplate redisTemplate;

    @Cacheable(cacheNames="WechatUser")
    public Result getWechatUser(){
        List<WechatUserDto> wechatUserList =wechatUserMapper.getWechatUser();
        if(!wechatUserList.isEmpty())
            return new Result<List<WechatUserDto>>().ok("成功",wechatUserList);
        else
            return new Result().serverError("失败");
    }





}
