package com.zbxj.wyz.mapper;




import com.zbxj.wyz.domain.Essay;
import com.zbxj.wyz.domain.dto.ChoiceToExamDto;
import com.zbxj.wyz.domain.dto.EssayDto;
import com.zbxj.wyz.domain.dto.EssayToExamDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EssayMapper {

    List<EssayDto> getEssayByCondition(String courseName, String essayDifficulty, String essayAudit);

    //增
    @Insert("insert into essay" +
            "(courseId,typeId,essayProblem,essayAnsw,essayParse,essayDifficulty,essayAudit)" +
            "values" +
            "(#{courseId},#{typeId},#{essayProblem},#{essayAnsw},#{essayParse},#{essayDifficulty},'未审核')")
    int addEssay(Essay essay);

    //删
    @Update("update essay set essayDelete = 'true' " +
            "where essayId=#{essayId}")
    int deleteEssay(Essay essay);


    @Update("update essay set essayProblem=#{essayProblem},\n" +
            "essayAnsw=#{essayAnsw},\n" +
            "essayParse=#{essayParse},\n" +
            "essayDifficulty=#{essayDifficulty}\n" +
            "where essayId=#{essayId}")
    int updateEssay(Essay essay);

    @Update("update essay set essayAudit='已审核' where essayId=#{essayId}")
    int auditEssay(Essay essay);

    List<EssayToExamDto> getAllEssayToExam(String courseName, String essayDifficulty);

}
