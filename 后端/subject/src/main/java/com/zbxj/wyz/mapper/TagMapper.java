package com.zbxj.wyz.mapper;


import com.zbxj.wyz.domain.Choice;
import com.zbxj.wyz.domain.Tag;
import com.zbxj.wyz.domain.dto.TagDto;
import com.zbxj.wyz.domain.dto.TagToTreeDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TagMapper {
    @Select("select tag.tagId,tag.tagName,tag.parentTagId,tag.tagColour,tag.tagAudit,t.tagName as parentTagName from tag\n" +
            "left join tag t on tag.parentTagId = t.tagId\n" +
            "where tag.courseId=#{courseId} and tag.tagAudit='已审核'")
    List<TagDto> getAllTag(int courseId);

    @Delete("delete from tag where tagId=#{tagId}")
    int deleteTag(Tag tag);

    @Insert("insert into tag(tagName,parentTagId,courseId,tagColour) values (#{tagName},#{parentTagId},#{courseId},#{tagColour})")
    int addTag(Tag tag);

    @Update("update tag set tagName=#{tagName},parentTagId=#{parentTagId},tagColour=#{tagColour} where tagId=#{tagId}")
    int updateTag(Tag tag);

    @Select("select tagName from tag where tagId = #{tagId}")
    String getTagNameById(int tagId);

    @Update("update tag set tagAudit = '已审核' where tagId = #{tagId}")
    int auditTag(Tag tag);

    @Select("select tag.tagId,tag.tagName,tag.tagColour,tag.tagAudit,t.tagName as parentTagName from tag\n" +
            "left join tag t on tag.parentTagId = t.tagId\n" +
            "where tag.courseId=#{courseId} and tag.tagAudit='未审核'")
    List<TagDto> getTagAuditList(int courseId);

    @Select("select tagId,tagName,parentTagId,tagColour from tag where courseId=#{courseId} and tag.tagAudit='已审核'")
    List<TagToTreeDto> getTagToTreeList(int courseId);
}
