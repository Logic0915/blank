package com.zbxj.wyz.mapper;

import com.zbxj.wyz.domain.Exam;
import com.zbxj.wyz.domain.dto.ExamDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ExamMapper {

    @Select("select examId,examName,courseName,examDate,totalTime,totalScore,state,examNote from exam\n" +
            "inner join course on exam.courseId = course.courseId\n" +
            "where courseName=#{courseName}")
    List<ExamDto> getExamByCourseName(String courseName);

    @Select("select examName from exam where examId = #{examId}")
    String getExamNameByExamId(int examId);

    @Options(useGeneratedKeys = true,keyProperty = "examId")//返回主键
    @Insert("insert into exam(examName,courseId,examDate,totalTime,totalScore,examNote)\n" +
            "values (#{examName},#{courseId},#{examDate},#{totalTime},#{totalScore},#{examNote})")
    int addExam(Exam exam);

    @Delete("delete from exam where examId = #{examId}")
    int deleteExam(Exam exam);

    @Update("update exam\n" +
            "set examName=#{examName},examDate=#{examDate},totalTime=#{totalTime},totalScore=#{totalScore},examNote=#{examNote}\n" +
            "where examId = #{examId}")
    int updateExam(Exam exam);
}
