package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.ExamData;
import com.zbxj.wyz.domain.dto.*;
import com.zbxj.wyz.service.ExamDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@ResponseBody
public class ExamDataController {

    @Autowired
    ExamDataService examDataService;

    @GetMapping("/examdata/choice/{examId}")
    public ResponseEntity getExamDataByChoice(@PathVariable("examId") String examId){
        return ResponseEntity.ok(examDataService.getExamDataChoice(examId));
    }

    @GetMapping("/examdata/trueorfalse/{examId}")
    public ResponseEntity getExamDataByTrueOrFalse(@PathVariable("examId") String examId){
        return ResponseEntity.ok(examDataService.getExamDataTrueOrFalse(examId));
    }

    @GetMapping("/examdata/essay/{examId}")
    public ResponseEntity getExamDataByEssay(@PathVariable("examId") String examId){
        return ResponseEntity.ok(examDataService.getExamDataEssay(examId));
    }

    @PostMapping("/examdata/addList")
    public ResponseEntity addProbelmListToExam(@RequestBody ExamDataDto examDataDto){
        return ResponseEntity.ok(examDataService.addProbelmListToExam(examDataDto.getExamId(),examDataDto.getUuIdList()));
    }

    @PostMapping("examdata/delete")
    public ResponseEntity deleteOneExamData(@RequestBody ExamData examData){
        return ResponseEntity.ok(examDataService.deleteOneExamData(examData));
    }

}
