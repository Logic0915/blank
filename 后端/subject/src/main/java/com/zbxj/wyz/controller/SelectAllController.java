package com.zbxj.wyz.controller;

import com.zbxj.wyz.domain.dto.ChoiceDto;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.service.ChoiceService;
import com.zbxj.wyz.service.SelectAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

//@RequestMapping(value = "/api")
@Controller
@ResponseBody
public class SelectAllController {
//    @Autowired
//    ChoiceService choiceService;

    @Autowired
    SelectAllService selectAllService;

    /**
     * 查
     * get访问案例：http://localhost:8081/search/数学/选择题/3/f
     * 根据科目、题型、难度、是否审核筛选题目列表
     * @param courseName 科目名：数学、英语、数据结构
     * @param typeName 题型名：选择题、判断题、问答题
     * @param difficulty 难度：1/2/3
     * @param audit 审核状态：t/f
     * @return 返回包含ChoiceDto、TrueOrFalseDto、EssayDto的列表
     */
    @GetMapping("/search/{courseName}/{typeName}/{difficulty}/{audit}")
    public ResponseEntity getQuestionByCondition(
            @PathVariable("courseName") String courseName,@PathVariable("typeName") String typeName,
            @PathVariable("difficulty") String difficulty,
            @PathVariable("audit") String audit){

        return ResponseEntity.ok(selectAllService.getQuestionByCondition(courseName, typeName, difficulty, audit));
    }

}
