package com.zbxj.wyz.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@TableName(value="trueorfalse")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrueOrFalse {
    //  @TableField(value = "goodsId")
    @TableId
    private Integer trueOrFalseId;
    private Integer courseId;
    private Integer typeId;

    private String trueOrFalseProblem;
    private String trueOrFalseAnsw;
    private String trueOrFalseParse;

    private String trueOrFalseDifficulty;
    private String trueOrFalseAudit;
    private String trueOrFalseDelete;

    public TrueOrFalse(int courseId, int typeId, String trueOrFalseProblem, String trueOrFalseAnsw, String trueOrFalseParse, String trueOrFalseDifficulty, String trueOrFalseAudit) {
        this.courseId = courseId;
        this.typeId = typeId;

        this.trueOrFalseProblem = trueOrFalseProblem;
        this.trueOrFalseAnsw = trueOrFalseAnsw;
        this.trueOrFalseParse = trueOrFalseParse;
        this.trueOrFalseDifficulty = trueOrFalseDifficulty;
        this.trueOrFalseAudit = trueOrFalseAudit;

    }
}
