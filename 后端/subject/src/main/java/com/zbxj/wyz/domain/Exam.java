package com.zbxj.wyz.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@TableName(value="exam")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Exam {
    @TableId
    private Integer examId;
    private String examName;
    private Integer courseId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime examDate;
    private Integer totalTime;
    private Integer TotalScore;
    private String state;
    private String examNote;
}
