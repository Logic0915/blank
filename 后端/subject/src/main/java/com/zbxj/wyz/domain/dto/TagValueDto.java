package com.zbxj.wyz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagValueDto {
    private Integer tagValueId;
    private String tagName;
    private String tagValue;
    private String tagValueAudit;
}
