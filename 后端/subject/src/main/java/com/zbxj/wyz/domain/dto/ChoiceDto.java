package com.zbxj.wyz.domain.dto;


import com.zbxj.wyz.domain.Choice;
import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * ChioceDto是数据传输对象,
 * 获取前端的courseName、typeName，
 * 通过getIdByCourseName（）、getIdByTypeName(chioceDto.getTypeName()，
 * 转化为courseId、typeId,
 * 然后结合其他数据，通过toChioce（）方法实现选择题数据的添加
 */
//@RedisHash("ChoiceDto")
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChoiceDto implements Serializable {//redis缓存需要的序列化
    private Integer choiceId;
    @Indexed
    private String courseName;
    @Indexed
    private String typeName;

    private String problem;
    private String difficulty;
    private String audit;

    private String choiceProblem;
    private String choiceOp1;
    private String choiceOp2;
    private String choiceOp3;
    private String choiceOp4;
    private String choiceAnsw;
    private String choiceParse;

    @Indexed
    private String choiceDifficulty;
    @Indexed
    private String choiceAudit;

    //将ChioceDto对象转换为Chioce对象
    public Choice toChioce(int courseId, int typeId, ChoiceDto choiceDto){
        return new Choice(
                courseId,typeId, choiceDto.getChoiceProblem(), choiceDto.getChoiceOp1(),
                choiceDto.getChoiceOp2(), choiceDto.getChoiceOp3(), choiceDto.getChoiceOp4(),
                choiceDto.getChoiceAnsw(), choiceDto.getChoiceParse(), choiceDto.getChoiceDifficulty(),choiceDto.getChoiceAudit()
        );
    }
}
