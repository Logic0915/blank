package com.zbxj.wyz.config;


import com.zbxj.wyz.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * 自定义一个过滤器，该过滤器用来对所有请求进行验证，判断请求的header中是否携带token，如果携带token则验证token的合法性。通过解析token获取用户名username，再从缓存中获取该username的用户信息，将用户信息存到SecurityContextHolder中。
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //获取token
        String token = request.getHeader("token");
        if (!StringUtils.hasText(token)) {
            //如果请求头中不包含token就直接放行
            filterChain.doFilter(request, response);

//            HttpServletResponse resp = (HttpServletResponse) response;
//            resp.sendRedirect("/login");

            return;
//            throw new RuntimeException("用户可能未登录");
        }
        //解析token
        String username;
        try {
            Claims claims = JwtUtil.parseJWT(token);//解析token
            username = claims.getSubject();//从token中获取用户名
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("token非法");
        }
        //从redis中获取用户信息
        String redisKey = "login_" + username;
        User loginUser =(User) redisTemplate.opsForValue().get(redisKey);//获取缓存中的用户信息
        if(Objects.isNull(loginUser)){//如果缓存没有数据，说明用户未登录
            throw new RuntimeException("用户未登录");
        }
        //将获取到的用户信息存入SecurityContextHolder
//UsernamePasswordAuthenticationToken构造方法的三个参数： principal一般指的是用户名，credentials一般指的是密码，authorities指权限
        UsernamePasswordAuthenticationToken authenticationToken =new UsernamePasswordAuthenticationToken(loginUser.getUsername(),loginUser.getPassword(),loginUser.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        //放行
        filterChain.doFilter(request, response);
    }
}
