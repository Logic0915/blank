package com.zbxj.wyz.mapper;


import com.zbxj.wyz.domain.dto.WechatUserDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface WechatUserMapper {
    @Select("select * from wechat_user")
    List<WechatUserDto> getWechatUser();
}
