package com.zbxj.wyz.controller;


import com.zbxj.wyz.domain.WechatUser;
import com.zbxj.wyz.domain.dto.Result;
import com.zbxj.wyz.service.WechatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
public class WechatUserController {

    @Autowired
    WechatUserService wechatUserService;

    @GetMapping("/wechatuser")
    public ResponseEntity getWechatUser(){
        return ResponseEntity.ok(wechatUserService.getWechatUser());
    }



}
