package com.zbxj.wyz.service;


import com.zbxj.wyz.domain.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SelectAllService {
    @Autowired
    ChoiceService choiceService;

    @Autowired
    EssayService essayService ;
    @Autowired
    TrueOrFalseService trueOrFalseService;

//    @Cacheable(cacheNames="QuestionByCondition")
    public Result getQuestionByCondition(String courseName,String typeName,String difficulty,String audit){
        //查询数据库
        List<Object> resultList = new ArrayList<>();
        if(typeName.equals("全部")){
            //查找选择题
            List<ChoiceDto> choiceList = choiceService.getChoiceByCondition(courseName,difficulty,audit);
            List<EssayDto> essayList = essayService.getEssayByCondition(courseName,difficulty,audit);
            List<TrueOrFalseDto> trueOrFalseList = trueOrFalseService.getTrueOrFalseByCondition(courseName,difficulty,audit);
            resultList.addAll(choiceList);
            resultList.addAll(trueOrFalseList);
            resultList.addAll(essayList);
        }
        else if(typeName.equals("选择题")){
            //查找选择题
            List<ChoiceDto> choiceList = choiceService.getChoiceByCondition(courseName,difficulty,audit);
            resultList.addAll(choiceList);
        }
        else if(typeName.equals("判断题")){
            //查找判断题
            List<TrueOrFalseDto> trueOrFalseList = trueOrFalseService.getTrueOrFalseByCondition(courseName,difficulty,audit);
            resultList.addAll(trueOrFalseList);
        }
        else if(typeName.equals("简答题")){
            //查找简答题
            List<EssayDto> essayList = essayService.getEssayByCondition(courseName,difficulty,audit);
            resultList.addAll(essayList);
        }

        //返回结果
        if(!resultList.isEmpty())
            return new Result<List<Object>>().ok("根据指定条件获取题目列表成功",resultList);
        else
            return new Result().serverError("根据指定条件获取题目列表失败,或筛选结果为空");
    }

}
